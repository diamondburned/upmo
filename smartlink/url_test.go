package smartlink

import "testing"

func TestParseURL(t *testing.T) {
	url := ParseURL(`https://canary.discordapp.com/channels/361910177961738242/403741067335434240/565534928381411349`)
	if url == nil {
		t.Fatal("Parsed URL is nil")
	}

	if url.Canary != true {
		t.Fatal("Canary failed")
	}

	if url.GuildID != "361910177961738242" {
		t.Fatal("GuildID failed")
	}

	if url.ChannelID != "403741067335434240" {
		t.Fatal("ChannelID failed")
	}

	if url.MessageID != "565534928381411349" {
		t.Fatal("MessageID failed")
	}
}
