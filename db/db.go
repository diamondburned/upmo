package db

import "github.com/asdine/storm"

func AcquireNode(node storm.Node, f func(node storm.Node) error) error {
	node, err := node.Begin(true)
	if err != nil {
		return err
	}

	defer node.Rollback()

	if err := f(node); err != nil {
		return err
	}

	return node.Commit()
}

func ReadNode(node storm.Node, f func(node storm.Node) error) error {
	node, err := node.Begin(false)
	if err != nil {
		return err
	}

	defer node.Rollback()

	if err := f(node); err != nil {
		return err
	}

	return nil
}
