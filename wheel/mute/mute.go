package mute

import (
	"errors"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/asdine/storm"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/upmo/db"
	"gitlab.com/diamondburned/upmo/wheel"
)

const (
	moduleName = "mute"
)

var node storm.Node

// Mute is the interface-pluggable struct containing methods
type Mute struct {
	GuildID  string `storm:"unique,id"`
	MuteRole string

	MutedUsers []string
}

var _ wheel.Action = (*Mute)(nil)

// Initialize sets the global database
func (m *Mute) Initialize(d storm.Node) error {
	node = d
	return nil
}

// Name returns the module name
func (m *Mute) Name() string {
	return moduleName
}

// Act acts on the user when the voting threshold was reached. The vote is ended and the logs are finalized.
// `node` is db.From("mute")
func (m *Mute) Act(ctx *exrouter.Context, vote *wheel.Vote) (string, error) {
	// Grab the latest Mute entry from the database
	if err := node.One("GuildID", vote.GuildID, m); err != nil {
		return "", err
	}

	// Give him the Muted role
	err := ctx.Ses.GuildMemberRoleAdd(
		vote.GuildID,
		vote.TargetUser,
		// This relies on the Setup() method
		m.MuteRole,
	)

	if err != nil {
		return "", err
	}

	err = db.AcquireNode(node, func(node storm.Node) error {
		// Add the muted user to the store
		m.MutedUsers = append(m.MutedUsers, vote.TargetUser)

		// Save the database
		if err := node.Save(m); err != nil && err != storm.ErrAlreadyExists {
			return err
		}

		return nil
	})

	if err != nil {
		return "", err
	}

	// Get the guild member
	mem, err := ctx.Member(ctx.Msg.GuildID, ctx.Msg.Author.ID)
	if err != nil {
		return "", err
	}

	var name = mem.Nick
	if mem.Nick == "" {
		name = mem.User.Username
	}

	// Return the muted message
	return "Muted " + name + " (userID: `" + mem.User.ID + "`)", nil
}

// Setup sets up the module
func (m *Mute) Setup(s *discordgo.Session, guildID, args string) error {
	if args == "" {
		return errors.New("Missing mute channel")
	}

	rls, err := s.GuildRoles(guildID)
	if err != nil {
		return err
	}

	var role *discordgo.Role

	for _, rl := range rls {
		if rl.ID == args || rl.Name == args {
			role = rl
			break
		}
	}

	if role == nil {
		return errors.New("Role not found")
	}

	return db.AcquireNode(node, func(node storm.Node) error {
		m.GuildID = guildID
		m.MuteRole = role.ID

		if err := node.Save(m); err != nil && err != storm.ErrAlreadyExists {
			return err
		}

		return nil
	})
}
