package wheel

import "github.com/asdine/storm"

var (
	node    storm.Node
	actions []Action
)

// Initialize initializes the logger
func Initialize(d *storm.DB) {
	node = d.From("wheel")
}

// AddActions adds actions
func AddActions(as ...Action) error {
	for _, a := range as {
		if err := a.Initialize(node.From(a.Name())); err != nil {
			return err
		}
	}

	actions = append(actions, as...)
	return nil
}
