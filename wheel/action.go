package wheel

import (
	"github.com/asdine/storm"
	"github.com/bwmarrin/discordgo"
)

// Action is an interface to plug in a module.
type Action interface {
	Initialize(storm.Node) error
	Name() string
	// Admin-only (UserisAdmin)
	Setup(s *discordgo.Session, guildID, args string) error
	// node.From(Guild.ID).From(Action.Name())
	Act(*discordgo.Session, *Vote) (string, error)
}
