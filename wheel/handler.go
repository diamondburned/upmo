package wheel

import (
	"strings"

	"github.com/Necroforger/dgrouter/exrouter"
)

// Handler handles exrouter contexts automatically.
func Handler(ctx *exrouter.Context) {
	if db == nil {
		ctx.Reply("wheel is uninitialized")
		return
	}

	if err := handler(ctx); err != nil {
		ctx.Reply("Error: `" + err.Error() + "`")
	}
}

func handler(ctx *exrouter.Context) (err error) {
	switch ctx.Args.Get(1) {
	case "list":
		var active = make([]string, len(actions))

		for i, a := range actions {
			active[i] = a.Name()
		}

		ctx.Reply("Active modules:" + strings.Join(active, "\n- "))

	case "setup":
		// This should take either
		// - $wheel setup reset
		// - $wheel setup [module]
		switch ctx.Args.Get(2) {
		case "":
			err = Setup(ctx.Ses, ctx.Msg.GuildID, ctx.Msg.ChannelID)
			if err != nil {
				return err
			}

			ctx.Reply("Setup completed. Please set the vote threshold, " +
				"the administrative roles and the wheel roles.")
		
		}

	case "vote":
	}

	return nil
}
