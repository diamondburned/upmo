package main

import (
	"image/png"
	"log"
	"os"
	"runtime/debug"
	"strings"
	"time"

	"github.com/asdine/storm"
	"github.com/asdine/storm/codec/protobuf"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/block"
	"gitlab.com/diamondburned/crawl-sticker/stickers"
	"gitlab.com/diamondburned/upmo/aocboard"
	"gitlab.com/diamondburned/upmo/compresspng"
	"gitlab.com/diamondburned/upmo/logger"
	"gitlab.com/diamondburned/upmo/metrics"
	"gitlab.com/diamondburned/upmo/nix"
	"gitlab.com/diamondburned/upmo/roles"
	"gitlab.com/diamondburned/upmo/smartlink"
	"gitlab.com/diamondburned/upmo/stalin"
	"gitlab.com/diamondburned/upmo/starboard"
	"gitlab.com/diamondburned/upmo/tags"
	"gitlab.com/diamondburned/upmo/webserver"
	bolt "go.etcd.io/bbolt"
)

const botPrefix = "$"

const (
	databasePath = "_data/"
	stickersPath = "stickers/output/"
)

var (
	d    *discordgo.Session
	stop chan struct{}
)

func init() {
	debug.SetGCPercent(400)
}

func main() {
	token := os.Getenv("BOT_TOKEN")
	if token == "" {
		panic("$BOT_TOKEN empty")
	}

	port := os.Getenv("PORT")
	if port == "" {
		port = "38471"
	}

	if err := os.MkdirAll(databasePath, 0600); err != nil {
		panic(err)
	}

	log.SetFlags(log.Llongfile | log.Ltime | log.Ldate)
	log.SetPrefix("\n  ")

	if os.Getenv("USER") != "root" {
		logger.Debug = true
	}

	db, err := storm.Open(
		databasePath+"upmo.db",
		storm.BoltOptions(0640, &bolt.Options{
			Timeout:      1 * time.Second,
			NoGrowSync:   true,
			FreelistType: bolt.FreelistArrayType,
		}),
		storm.Codec(protobuf.Codec),
	)

	if err != nil {
		panic(err)
	}

	defer db.Close()

	log.Println("Opened database at ./upmo.db.")

	// Make a new Discordgo session

	d, err = discordgo.New("Bot " + token)
	if err != nil {
		panic(err)
	}

	// Start metrics
	metrics.OAuthSettings.ClientID = os.Getenv("CLIENT_ID")
	metrics.OAuthSettings.ClientSecret = os.Getenv("CLIENT_SECRET")
	metrics.OAuthSettings.Host = os.Getenv("REDIRECT_URI")

	if err = metrics.Initialize(d, db); err != nil {
		panic(err)
	}

	defer metrics.Stop()

	log.Println("Metrics started successfully.")

	// Make a router
	r := NewRouter()

	// Initialize the tag system
	tags.Initialize(
		databasePath,
		storm.BoltOptions(0640, &bolt.Options{
			Timeout:      1 * time.Second,
			NoGrowSync:   true,
			FreelistType: bolt.FreelistArrayType,
		}),
		storm.Codec(protobuf.Codec),
	)

	// Initialize starboard
	starboard.Initialize(db)
	d.AddHandler(starboard.Handler)

	// Innitialize stalin
	stalin.Initialize(db)
	d.AddHandler(stalin.HandlerCreate)
	d.AddHandler(stalin.HandlerEdit)

	// Initialize the Logger
	logger.Initialize(db)

	// Initialize roles
	roles.Initialize(db)

	// Initialize Advent of Code leaderboard
	aocboard.Initialize(db)

	// Initialize Nix
	nix.Initialize()

	// Initialize Stickers
	compresspng.CompressionLevel = png.BestCompression
	if err := stickers.Refresh(stickersPath); err != nil {
		log.Println(err)
	}

	// Add a messageCreate handler for commands

	d.AddHandler(func(ses *discordgo.Session, msg *discordgo.MessageCreate) {
		if msg.Author.ID == d.State.User.ID {
			return
		}

		if msg.Author.Bot {
			return
		}

		/*
			// If:
			//   - The message isn't from a bot
			//   - The message isn't a bot command
			//   - The message is longer than 10 runes (UTF-8 friendly characters)
			if !strings.HasPrefix(msg.Content, botPrefix) &&
				len([]rune(msg.Content)) > 22 {

				go m.AddEventFromUser(msg.Author, msg.GuildID)
			}
		*/

		go stickersHandler(ses, msg)

		switch {
		case strings.HasPrefix(msg.Content, botPrefix):
			r.FindAndExecute(ses, botPrefix, ses.State.User.ID, msg.Message)
		case strings.HasPrefix(msg.Content, "https://"):
			smartlink.Handler(ses, msg)
		}
	})

	// Connect to Discord
	if err := d.Open(); err != nil {
		log.Fatalln("::FATAL:: Failed to open Discord:", err)
	}

	defer d.Close()

	log.Printf("Connected to Discord as %s.\n", d.State.User.Username)

	// Start the webserver
	go webserver.ListenAndServe("127.0.0.1:" + port)
	log.Printf("Listening to HTTP at 127.0.0.1:" + port)

	ready := make(chan struct{})

	d.AddHandlerOnce(func(d *discordgo.Session, r *discordgo.Ready) {
		ready <- struct{}{}
		close(ready)
	})

	log.Println("Waiting for Discord to finish up...")
	<-ready

	// Block until end signal or stop signal

	select {
	case <-block.Ch():
	case <-stop:
		log.Println("Killswitch received")
	}

	log.Println("Stopping")
}
