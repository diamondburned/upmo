package main

import (
	"fmt"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/upmo/logger"
)

func sendToDiscord(c *exrouter.Context, content interface{}) (m *discordgo.Message, e error) {
	switch content := content.(type) {
	case error:
		m, e = c.Ses.ChannelMessageSend(c.Msg.ChannelID, content.Error())
	case string:
		m, e = c.Ses.ChannelMessageSend(c.Msg.ChannelID, content)
	case *discordgo.MessageEmbed:
		m, e = c.Ses.ChannelMessageSendEmbed(c.Msg.ChannelID, content)
	case *discordgo.MessageSend:
		m, e = c.Ses.ChannelMessageSendComplex(c.Msg.ChannelID, content)
	default:
		m, e = c.Reply(content)
	}

	if e != nil {
		logger.Error(c.Ses, c.Msg.GuildID, e)
	}

	return m, e
}

func errToDiscord(c *exrouter.Context, err error) (m *discordgo.Message, e error) {
	go logger.Error(c.Ses, c.Msg.GuildID, err)

	var errString string

	switch err := err.(type) {
	case *discordgo.RESTError:
		if err.Message != nil && err.Message.Message != "" {
			errString =
				fmt.Sprintf("%d: %s\n", err.Message.Code, err.Message.Message)
			break
		}

		errString = err.Error()
	default:
		errString = err.Error()
	}

	return c.Ses.ChannelMessageSend(c.Msg.ChannelID, "Error: `"+errString+"`")
}
