// Package move provides the stuff to move conversations across channels.
// a bot that moves conversation
// 1. $fuckoff @personA @personB @personC to #devnull
// 2. bot cuts their messages to devnull
// 3. bot temp mutes them from that channel for 2 minutes
package move

// Denying a member:
// ChannelPermissionSet(chID, userID, "member", 0, deny)
// ChannelPermissionDelete(chID, userID)

// Crawling:
// Get 200 messages, iterate over them, filter
// If left over message < 100, repeat
// Send the new messages over, trim 2000 chars
// BulkDelete old ones in 100s
// Deny old members for 2 minutes

// Usage:
// $move allow - list all roles allowed (name - ID)
// $move allow Role Name - allow role (should accept role mention too)
// $move @user1 @user2 to #devnull (syntax requires to)
