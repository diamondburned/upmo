package widgets

import (
	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
)

func FindDMChannel(s *discordgo.Session, userID string) (*discordgo.Channel, error) {
	for _, c := range s.State.PrivateChannels {
		if len(c.Recipients) == 1 && c.Recipients[0].ID == userID {
			return c, nil
		}
	}

	c, err := s.UserChannelCreate(userID)
	if err != nil {
		return c, errors.Wrap(err, "Failed to create a new DM channel")
	}

	s.State.ChannelAdd(c)
	return c, nil
}
