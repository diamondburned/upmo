package widgets

import (
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
)

func UserInputPrompt(s *discordgo.Session, userID, channelID string,
	timeout time.Duration, prompt string) (*discordgo.Message, error) {

	if _, err := s.ChannelMessageSend(channelID, prompt); err != nil {
		return nil, errors.Wrap(err, "Failed to DM <@"+userID+">")
	}

	return UserInput(s, userID, channelID, timeout)
}

func UserInput(s *discordgo.Session, userID, channelID string,
	timeout time.Duration) (*discordgo.Message, error) {

	var (
		input = make(chan *discordgo.Message)
		after = time.After(timeout)
	)

	die := s.AddHandler(func(d *discordgo.Session, m *discordgo.MessageCreate) {
		if m.Author.ID == userID && m.ChannelID == channelID {
			input <- m.Message
		}
	})

	defer close(input)
	defer die()

	select {
	case m := <-input:
		return m, nil
	case <-after:
		return nil, errors.New("Prompt timed out.")
	}
}
