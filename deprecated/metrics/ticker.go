package metrics

import (
	"math"
	"time"

	"gitlab.com/diamondburned/clocker"
)

func (m *Metrics) startTicker() {
	var (
		ticker = clocker.NewTicker(m.opts.Timeframe)

		currentguilds = map[string]map[string]uint{}
		lastguilds    = map[string]map[string]uint{}
	)

	defer close(m.update)
	defer ticker.Stop()

	for {
		select {
		case t := <-ticker.C:
			m.lastupdated = &t
		case <-m.update:
			t := time.Now()
			m.lastupdated = &t
		case <-m.stop:
			return
		}

		m.Lock()
		{ // copy the frame so it doesn't interrupt
			lastguilds = currentguilds
			currentguilds = m.framedata
			m.framedata = map[string]map[string]uint{}
		}
		m.Unlock()

		tx, err := m.node.Begin(true)
		if err != nil {
			m.Logln(err)
			return
		}

		defer tx.Rollback()

		for guildID, current := range currentguilds {
			var (
				resetStreaks []string
				users        []*User
			)

			gdb := tx.From(guildID)

			if gdb == nil {
				m.Logln("[metrics]", guildID, ErrNilNode)
				continue
			}

			if err := gdb.All(&users); err != nil {
				gdb.Init(&User{})
			}

			// ok is true if the last frame has this guild. In
			// the case where the last frame does not have the
			// guild, there would be no one to reset.
			if last, ok := lastguilds[guildID]; ok && len(users) > 0 {
				// Check for users who became inactive
				for uID := range last {
					// If the user in the last frame is NOT
					// in the current frame
					if _, ok := current[uID]; !ok {
						// Reset their combo to 1
						if u := findUser(users, uID); u != nil {
							u.Combo = 1
							if err := gdb.Save(u); err != nil {
								m.Logln(err)
							}
						}

						// There's no need to add their score, as
						// they're inactive

						// Add them into the array of users that was
						// reset
						resetStreaks = append(resetStreaks, uID)
					}
				}
			}

		Current: // Check for users who are still active
			for uID, count := range current {
				for _, resetID := range resetStreaks {
					// If the user is in the list of users to be reset,
					if resetID == uID {
						// then skip them
						continue Current
					}
				}

				// Here, the user could either be someone who is already in
				// the list, or is completely new.

				// We're having setScore() return false if the user is not
				// found. Then, we could manually append them.

				u := findUser(users, uID)
				if u != nil {
					if u.Combo < 16 {
						u.Combo++
					}

					u.Score += 0.015 * float64(count) * math.Pow(float64(u.Combo), 1.25)
				} else {
					u = m.GetUserDefault(uID)
				}

				if err := gdb.Save(u); err != nil {
					m.Logln(err)
				}
			}
		}

		if err := tx.Commit(); err != nil {
			m.Logln(err)
		}
	}
}

func findUser(users []*User, uID string) *User {
	for _, u := range users {
		if u.ID == uID {
			return u
		}
	}

	return nil
}
