package main

import (
	"fmt"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/bwmarrin/discordgo"
	"github.com/dustin/go-humanize"
	"github.com/pkg/errors"
)

func routerXP(ctx *exrouter.Context) {
	if ctx.Msg.GuildID == "" {
		return
	}

	const userLimit = 100

	users, err := m.QueryUsers(ctx.Msg.GuildID, userLimit)
	if err != nil {
		sendToDiscord(ctx, errors.Wrap(err, "Failed to query users"))
		return
	}

	if len(users) == 0 {
		sendToDiscord(ctx, "No activities recorded yet.")
		return
	}

	p := newPaginator(ctx)

	if len(users) > userLimit {
		users = users[:userLimit]
	}

	for j := 0; j < len(users); j += 5 {
		embed := &discordgo.MessageEmbed{
			Title:  "XP Leaderboard",
			Color:  0x34be5b,
			Fields: make([]*discordgo.MessageEmbedField, 0, 5),
		}

		for i := j; i-j < 10 && i < len(users); i++ {
			m, _ := ctx.Member(ctx.Msg.GuildID, users[i].ID)
			if m == nil {
				m = &discordgo.Member{
					Nick: users[i].ID,
					User: &discordgo.User{
						Username:      "invalid user",
						Discriminator: "0000",
					},
				}
			}

			embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
				Name:  fmtUsername(m),
				Value: fmt.Sprintf("%.3f XP", users[i].Score),
			})
		}

		p.Add(embed)
	}

	p.SetPageFooters()
	if err := p.Spawn(); err != nil {
		errToDiscord(ctx, err)
	}
}

func routerMe(ctx *exrouter.Context) {
	if ctx.Msg.GuildID == "" {
		return
	}

	var uID = ctx.Msg.Author.ID
	if len(ctx.Msg.Mentions) > 0 {
		uID = ctx.Msg.Mentions[0].ID
	}

	user, err := m.Query(ctx.Msg.GuildID, uID)
	if err != nil {
		sendToDiscord(ctx, errors.Wrap(err, "Failed to query user"))
		return
	}

	if user == nil {
		sendToDiscord(ctx, "User not found.")
		return
	}

	mem, err := ctx.Member(ctx.Msg.GuildID, uID)
	if err != nil {
		sendToDiscord(ctx, errors.Wrap(err, "Failed to get member"))
		return
	}

	var lastUpdated = "nil"
	if t := m.GetLastUpdated(); t != nil {
		lastUpdated = humanize.Time(*t)
	}

	embed := &discordgo.MessageEmbed{
		Author: &discordgo.MessageEmbedAuthor{
			Name:    fmtUsername(mem),
			IconURL: mem.User.AvatarURL("32"),
		},
		Fields: []*discordgo.MessageEmbedField{
			&discordgo.MessageEmbedField{
				Name:   "Points",
				Value:  fmt.Sprintf("%.3f XP", user.Score),
				Inline: true,
			},
			&discordgo.MessageEmbedField{
				Name:   "Current streaks",
				Value:  fmt.Sprintf("%dx", user.Combo),
				Inline: true,
			},
			&discordgo.MessageEmbedField{
				Name:   "Last updated",
				Value:  lastUpdated,
				Inline: true,
			},
		},
	}

	sendToDiscord(ctx, embed)
}

func routerResetXP(ctx *exrouter.Context) {
	if len(ctx.Msg.Mentions) < 1 {
		sendToDiscord(ctx, "Missing mention for the target user (TODO: make this reset ALL metrics)")
		return
	}

	var ids = make([]string, len(ctx.Msg.Mentions))
	for i, m := range ctx.Msg.Mentions {
		ids[i] = m.ID
	}

	if err := m.Reset(ctx.Msg.GuildID, ids...); err != nil {
		errToDiscord(ctx, err)
		return
	}

	sendToDiscord(ctx, fmt.Sprintf("Reset metrics for %d members successfully", len(ids)))
}

func routerReindexXP(ctx *exrouter.Context) {
	sendToDiscord(ctx, "Reindexing...")

	if err := m.Reindex(ctx.Msg.GuildID); err != nil {
		errToDiscord(ctx, err)
		return
	}

	sendToDiscord(ctx, "Done.")
}

func routerDropMet(ctx *exrouter.Context) {
	if err := m.DropGuild(ctx.Msg.GuildID); err != nil {
		errToDiscord(ctx, err)
		return
	}

	sendToDiscord(ctx, "Dropped all metrics for this guild.")
}
