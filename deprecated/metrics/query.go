package metrics

import (
	"time"

	"github.com/asdine/storm"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/upmo/db"
)

// ErrNilNode is returned when the node is nil
var ErrNilNode = errors.New("Node for guild is nil")

// Query queries the SQL database for a user
func (m *Metrics) Query(guildID, userID string) (*User, error) {
	gdb := m.node.From(guildID)

	if gdb == nil {
		return nil, ErrNilNode
	}

	var u User

	if err := gdb.One("ID", userID, &u); err != nil {
		return nil, err
	}

	return &u, nil
}

func (m *Metrics) Reindex(guildID string) error {
	gdb := m.node.From(guildID)

	if gdb == nil {
		return ErrNilNode
	}

	m.reindexMu.Lock()

	t, ok := m.reindexing[guildID]
	if ok {
		m.reindexMu.Unlock()
		return errors.New(
			"Bot is already re-indexing for " + time.Now().Sub(t).String())
	}

	m.reindexing[guildID] = time.Now()
	m.reindexMu.Unlock()

	return db.AcquireNode(gdb, func(gdb storm.Node) (err error) {
		defer func() {
			if recover() != nil {
				if err == nil {
					err = errors.New("Fatal error occured")
				} else {
					err = errors.Wrap(err, "Fatal error occured")
				}
			}

			m.reindexMu.Lock()
			delete(m.reindexing, guildID)
			m.reindexMu.Unlock()
		}()

		err = gdb.ReIndex(&User{})
		return
	})
}

// QueryUsers queries the SQL database for top users
func (m *Metrics) QueryUsers(guildID string, limit int) ([]*User, error) {
	gdb := m.node.From(guildID)

	if gdb == nil {
		return nil, ErrNilNode
	}

	var users []*User

	if err := gdb.AllByIndex("Score", &users,
		storm.Reverse(), storm.Limit(limit)); err != nil {

		return nil, err
	}

	return users, nil
}
