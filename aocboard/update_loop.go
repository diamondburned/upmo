package aocboard

import (
	"log"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/diamondburned/clocker"
	"gitlab.com/diamondburned/upmo/logger"
)

var UpdateDuration = 30 * time.Minute

func StartLoop() (stop func()) {
	stopCh := make(chan struct{})

	go func() {
		ticker := clocker.NewTicker(UpdateDuration)
		defer ticker.Stop()

		for {
			select {
			case <-stopCh:
				close(stopCh)
				return
			case <-ticker.C:
				if ses == nil {
					break
				}

				updateAll()
			}
		}
	}()

	return func() {
		stopCh <- struct{}{}
	}
}

func updateAll() {
	var guildSettings []Settings
	if err := node.All(&guildSettings); err != nil {
		log.Println("AOC updateAll database failed:", err)
	}

	for _, s := range guildSettings {
		_, err := s.UpdateEmbed()
		if err != nil {
			logger.Error(ses, s.GuildID,
				errors.Wrap(err, "Failed to get leaderboard"))
			continue
		}

		if err := s.UpdateMessage(ses); err != nil {
			logger.Error(ses, s.GuildID, err)
			continue
		}

		// Boy do I love using sleep blindly to get over rate limiters
		// that I have no idea about
		time.Sleep(time.Second / 2)
	}
}
