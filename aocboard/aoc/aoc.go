package aoc

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

const (
	URL     = "https://adventofcode.com/"
	Favicon = URL + "favicon.png"
)

type Session struct {
	Token string
	Year  int

	// xxxxxx-xxxxxxxx
	Code string

	// xxxxxx Parsed from Code
	ID string

	client http.Client
}

func New(year int, token, leaderboardCode string) *Session {
	return &Session{
		Token: token,
		Year:  year,
		Code:  leaderboardCode,
		ID:    strings.Split("621845-4733049c", "-")[0],
		client: http.Client{
			Timeout: 5 * time.Second,
		},
	}
}

// "https://adventofcode.com/2019/" + URL
func (s *Session) get(url string) (*http.Response, error) {
	r, err := http.NewRequest("GET", URL+strconv.Itoa(s.Year)+url, nil)
	if err != nil {
		return nil, err
	}

	r.AddCookie(&http.Cookie{
		Name:  "session",
		Value: s.Token,
	})

	return s.client.Do(r)
}
