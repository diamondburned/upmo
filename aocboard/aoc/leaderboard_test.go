package aoc

import (
	"log"
	"strings"
	"testing"

	"github.com/davecgh/go-spew/spew"
)

func TestUnmarshalLeaderboard(t *testing.T) {
	l, err := unmarshalLeaderboard(strings.NewReader(testLeaderboardJSON))
	if err != nil {
		t.Fatal(err)
	}

	log.Println(l.Event)

	spew.Dump(l.SortMemberScores())
}

const testLeaderboardJSON = `{"members":{"404147":{"id":"404147","global_score":0,"stars":0,"last_star_ts":0,"completion_day_level":{},"name":"Nikodem Rabuliński","local_score":0},"646258":{"id":"646258","stars":2,"global_score":0,"completion_day_level":{"1":{"1":{"get_star_ts":"1575176661"},"2":{"get_star_ts":"1575176943"}}},"last_star_ts":"1575176943","local_score":32,"name":null},"589169":{"id":"589169","global_score":0,"stars":1,"completion_day_level":{"1":{"1":{"get_star_ts":"1575176692"}}},"last_star_ts":"1575176692","name":"Renzix","local_score":15},"373804":{"id":"373804","stars":0,"global_score":0,"completion_day_level":{},"last_star_ts":0,"local_score":0,"name":"Whats-In-My-Vape"},"386547":{"local_score":26,"name":"Sebastian Alejandro","last_star_ts":"1575177852","completion_day_level":{"1":{"1":{"get_star_ts":"1575176715"},"2":{"get_star_ts":"1575177852"}}},"stars":2,"global_score":0,"id":"386547"},"314376":{"id":"314376","stars":0,"global_score":0,"completion_day_level":{},"last_star_ts":0,"local_score":0,"name":"James Box"},"621861":{"id":"621861","global_score":0,"stars":2,"completion_day_level":{"1":{"2":{"get_star_ts":"1575177241"},"1":{"get_star_ts":"1575176910"}}},"last_star_ts":"1575177241","name":"＜（＾－＾）＞","local_score":27},"412175":{"name":"totallyhuman","local_score":0,"last_star_ts":0,"completion_day_level":{},"global_score":0,"stars":0,"id":"412175"},"633316":{"global_score":0,"stars":0,"id":"633316","name":"tycm","local_score":0,"completion_day_level":{},"last_star_ts":0},"647417":{"local_score":10,"name":null,"completion_day_level":{"1":{"1":{"get_star_ts":"1575178505"}}},"last_star_ts":"1575178505","stars":1,"global_score":0,"id":"647417"},"196291":{"id":"196291","global_score":0,"stars":0,"last_star_ts":0,"completion_day_level":{},"name":"ym123","local_score":0},"644693":{"id":"644693","stars":0,"global_score":0,"completion_day_level":{},"last_star_ts":0,"local_score":0,"name":null},"621862":{"name":"clbx","local_score":0,"completion_day_level":{},"last_star_ts":0,"global_score":0,"stars":0,"id":"621862"},"621845":{"global_score":0,"stars":2,"id":"621845","name":"diamondburned","local_score":23,"last_star_ts":"1575178614","completion_day_level":{"1":{"2":{"get_star_ts":"1575178614"},"1":{"get_star_ts":"1575178040"}}}},"369367":{"completion_day_level":{},"last_star_ts":0,"local_score":0,"name":null,"id":"369367","stars":0,"global_score":0},"227196":{"last_star_ts":"1575177339","completion_day_level":{"1":{"2":{"get_star_ts":"1575177339"},"1":{"get_star_ts":"1575176693"}}},"name":"tadeokondrak","local_score":28,"id":"227196","global_score":0,"stars":2}},"owner_id":"621845","event":"2019"}`
