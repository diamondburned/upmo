package aoc

import (
	"fmt"
	"io"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/pkg/errors"
)

const TotalDays = 25

// Range 1-25, 0 is not started/in the future
func (s *Session) CalculateLatestDay() int {
	est := time.Now().In(EST)

	switch {
	case est.Year() == s.Year:
		if est.Month() == time.December {
			day := est.Day()
			if day > TotalDays {
				return TotalDays
			}

			return day
		}

		// We're in the same year, but it's not December yet :(
		return 0

	case est.Year() > s.Year:
		// If we're after the session's Year, this means the Year is in the past. We
		// return the last day.
		return TotalDays

	case est.Year() < s.Year:
		// Vice versa, the Year is in the future.
		return 0
	}

	// Honestly this shouldn't even be possible
	return 0
}

func (s *Session) GetLatestDay() (int, error) {
	r, err := s.get("")
	if err != nil {
		return 0, err
	}

	if r.StatusCode != 200 {
		return 0, fmt.Errorf("Unexpected HTTP status %d",
			r.StatusCode)
	}

	defer r.Body.Close()

	return parseLatestDay(r.Body)
}

func parseLatestDay(r io.Reader) (int, error) {
	doc, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		return 0, errors.Wrap(err, "Failed to parse AOC page")
	}

	days := doc.Find("pre.calendar > *")
	if days == nil {
		return 0, errors.New("Can't find 'pre.calendar > *'")
	}

	return days.Length(), nil
}
