package aocboard

import (
	"time"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/asdine/storm"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/upmo/db"
	"gitlab.com/diamondburned/upmo/logger"
	"gitlab.com/diamondburned/upmo/middlewares"
)

func finalize(ctx *exrouter.Context) {
	middlewares.With(ctx, _finalize, middlewares.AdminOnly)
}

func _finalize(ctx *exrouter.Context) {
	var s Settings
	if err := node.One("GuildID", ctx.Msg.GuildID, &s); err != nil {
		if err == storm.ErrNotFound {
			ctx.Reply("Guild not found")
			return
		}

		logger.Error(ctx.Ses, ctx.Msg.GuildID,
			errors.Wrap(err, "AOC finalize error"))
		ctx.Reply("Unknown error: " + err.Error())
		return
	}

	// Set pill color to red
	if s.Embed != nil {
		s.Embed.Color = 0xcc0000
		s.Embed.Footer.Text = "Finalized"
		s.Embed.Timestamp = time.Now().Format(time.RFC3339)
		s.UpdateMessage(ctx.Ses)
	}

	err := db.AcquireNode(node, func(node storm.Node) error {
		return node.DeleteStruct(&s)
	})

	if err != nil {
		logger.Error(ctx.Ses, ctx.Msg.GuildID,
			errors.Wrap(err, "AOC finalize error"))
		ctx.Reply("Error removing database")
		return
	}

	ctx.Reply("Finalized.")
}
