package aocboard

import (
	"time"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/asdine/storm"
	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/upmo/aocboard/aoc"
	"gitlab.com/diamondburned/upmo/db"
	"gitlab.com/diamondburned/upmo/middlewares"
	"gitlab.com/diamondburned/upmo/widgets"
)

const tokenHelp = `Please paste your Session token here.

To find the token on Chrome, do:
	1. Enter the Developer Console (Ctrl-Shift-C)
	2. Go to [Application]
	3. Go to Storage -> Cookies -> <https://adventofcode.com>
	4. Find the cookie named "session" and copy its value`

func setup(ctx *exrouter.Context) {
	// Check if exists
	var s Settings
	if err := node.One("GuildID",
		ctx.Msg.GuildID, &s); err != storm.ErrNotFound {

		ctx.Reply("AOC Settings already exists, please remove.")
		return
	}

	var (
		leaderboard   = ctx.Args.Get(2)
		targetChannel = ctx.Args.Get(3)
	)

	if leaderboard == "" || targetChannel == "" {
		ctx.Reply(help)
		return
	}

	// Parse the channel ID
	targetChannel = targetChannel[2 : len(targetChannel)-1]

	middlewares.With(ctx, func(ctx *exrouter.Context) {

		if err := Setup(ctx.Ses, ctx.Msg.GuildID, ctx.Msg.ChannelID,
			ctx.Msg.Author.ID, leaderboard, targetChannel); err != nil {

			ctx.Reply("Error setting up: " + err.Error())
		}

	}, middlewares.AdminOnly, limiter)
}

func Setup(d *discordgo.Session, guildID, channelID, userID,
	leaderboard, targetChannel string) error {

	// We DM the user and wait for them to give us the token
	dm, err := widgets.FindDMChannel(d, userID)
	if err != nil {
		return err
	}

	tokenReply, err := widgets.UserInputPrompt(d, userID, dm.ID,
		30*time.Second, tokenHelp)
	if err != nil {
		return err
	}

	// Get the Advent of Code session
	ses := aoc.New(time.Now().Year(), tokenReply.Content, leaderboard)

	settings := &Settings{
		GuildID:           guildID,
		TargetChannel:     targetChannel,
		InitializerUserID: userID,

		AOC:         ses,
		Leaderboard: leaderboard,
	}

	// Construct an embed
	embed, err := settings.UpdateEmbed()
	if err != nil {
		return errors.Wrap(err, "Failed to get AOC leaderboard")
	}

	// Send the embed
	m, err := d.ChannelMessageSendEmbed(targetChannel, embed)
	if err != nil {
		return errors.Wrap(err, "Failed to send an embed to the target channel")
	}

	// Save the settings
	settings.MessageID = m.ID

	err = db.AcquireNode(node, func(node storm.Node) error {
		return node.Save(settings)
	})

	if err != nil {
		return errors.Wrap(err, "Failed to save AOC settings")
	}

	return nil
}
