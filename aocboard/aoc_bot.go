package aocboard

import (
	"fmt"
	"strconv"
	"time"

	"github.com/Necroforger/dgrouter/exmiddleware"
	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/asdine/storm"
	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/upmo/aocboard/aoc"
	"gitlab.com/diamondburned/upmo/db"
	"gitlab.com/diamondburned/upmo/logger"
	"gitlab.com/diamondburned/upmo/middlewares"
)

var (
	node storm.Node
	ses  *discordgo.Session
)

func Initialize(d *storm.DB) {
	node = d.From("aoc")
	StartLoop()
}

// TODO: Add a channel, remove (or finalize) AOC on command

// Used only for update
var limiter = exmiddleware.UserCooldown(
	15*time.Minute,
	func(ctx *exrouter.Context) {
		ctx.Reply("Not yet...")
	},
)

const help = `Usage:
	aoc update
	aoc init <leaderboard> <targetChannel>
	aoc finalize - freezes the leaderboard and stop updating
`

func Handler(ctx *exrouter.Context) {
	ses = ctx.Ses

	if node == nil {
		ctx.Reply("Database not initialized.")
		return
	}

	if ctx.Msg.GuildID == "" {
		ctx.Reply("Command has to be run inside a guild.")
		return
	}

	switch ctx.Args.Get(1) {
	case "update":
		update(ctx)
	case "init":
		setup(ctx)
	case "finalize":
		finalize(ctx)
	default:
		ctx.Reply(help)
		return
	}
}

func update(ctx *exrouter.Context) {
	var s Settings

	if err := node.One("GuildID", ctx.Msg.GuildID, &s); err != nil {
		if err == storm.ErrNotFound {
			ctx.Reply("Run `aoc init` first.")
			return
		}
	}

	middlewares.With(ctx, func(ctx *exrouter.Context) {

		if _, err := s.UpdateEmbed(); err != nil {
			logger.Error(ses, s.GuildID, err)
			ctx.Reply("Failed to manually update: " + err.Error())

			return
		}

		if err := s.UpdateMessage(ctx.Ses); err != nil {
			logger.Error(ses, s.GuildID, err)
			ctx.Reply("Failed to manually update: " + err.Error())
		}

		ctx.Reply("Done.")

	}, limiter)
}

type Settings struct {
	GuildID       string `storm:"id"`
	TargetChannel string `storm:"unique"`

	InitializerUserID string

	Embed     *discordgo.MessageEmbed
	MessageID string
	Updated   bool // true when manually updated

	AOC         *aoc.Session
	Leaderboard string
}

func (s *Settings) UpdateMessage(ses *discordgo.Session) error {
	_, err := ses.ChannelMessageEditEmbed(s.TargetChannel,
		s.MessageID, s.Embed)
	if err != nil {
		return fmt.Errorf("Failed to edit old embed in <#%s>, message ID %s",
			s.TargetChannel, s.MessageID)
	}

	// Save the Embed
	return db.AcquireNode(node, func(node storm.Node) error {
		return node.Save(s)
	})
}

func (s *Settings) UpdateEmbed() (*discordgo.MessageEmbed, error) {
	var latestDay = s.AOC.CalculateLatestDay()

	if s.Embed == nil {
		s.Embed = &discordgo.MessageEmbed{}
	}

	// The embed.
	if s.Embed.Author == nil {
		s.Embed.Author = &discordgo.MessageEmbedAuthor{
			Name:    "Advent of Code Leaderboard",
			URL:     aoc.URL,
			IconURL: aoc.Favicon,
		}
	}

	if s.Embed.Footer == nil {
		s.Embed.Footer = &discordgo.MessageEmbedFooter{
			Text: "Updated",
		}
	}

	s.Embed.Title = s.Leaderboard
	s.Embed.URL = s.AOC.LeaderboardURL()
	s.Embed.Color = 0x00cc00
	s.Embed.Timestamp = time.Now().Format(time.RFC3339)

	if latestDay == 0 {
		s.Embed.Description = "It's not Advent of Code, yet!"
		return s.Embed, nil
	}

	// Grab the leaderboard
	l, err := s.AOC.GetLeaderboard()
	if err != nil {
		return nil, errors.Wrap(err, "Failed to get leaderboard")
	}

	// Start creating the AOC leaderboard
	fields := formatAOCLeaderboard(s.AOC, l, latestDay)

	switch {
	case len(fields) == 0:
		s.Embed.Description = "Nobody is here :("
	default:
		s.Embed.Description = fmt.Sprintf(
			"[Day %d / %d](https://adventofcode.com/%d/day/%d)",
			latestDay, aoc.TotalDays, s.AOC.Year, latestDay,
		)
		s.Embed.Fields = fields
	}

	return s.Embed, nil
}

func formatAOCLeaderboard(s *aoc.Session, lb *aoc.Leaderboard, latestDay int,
) []*discordgo.MessageEmbedField {

	ranks := lb.SortMemberScores()

	var fields = make([]*discordgo.MessageEmbedField, 0, 12)

	for i := 0; i < 12; i++ {
		// Already have fields for everyone in the leaderboard.
		if i >= len(ranks) {
			break
		}

		var name = ranks[i].Name
		if name == "" {
			name = "#" + ranks[i].ID
		}

		fields = append(fields, &discordgo.MessageEmbedField{
			Name: strconv.Itoa(i+1) + ". " + name,
			Value: fmt.Sprintf(
				"Completed %d/%d\n⭐ %d (%d)",
				ranks[i].DaysCompleted.Total(), latestDay,
				ranks[i].Stars,
				ranks[i].LocalScore,
			),
			Inline: true,
		})
	}

	return fields
}
