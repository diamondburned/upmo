package main

import (
	"strings"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/upmo/logger"
	"gitlab.com/diamondburned/upmo/roles"
)

func routerRoles(ctx *exrouter.Context) {
	var err error

	switch ctx.Args.Get(1) {
	case "split":
		r := routerRolesMatchRole(ctx)
		if r == nil {
			sendToDiscord(ctx, "No roles matched.")
			return
		}

		err = roles.SplitRegion(d, ctx.Msg.GuildID, r)
	case "allow":
		r := routerRolesMatchRole(ctx)
		if r == nil {
			sendToDiscord(ctx, "No roles matched.")
			return
		}

		var b bool
		switch ctx.Args.Get(3) {
		case "true":
			b = true
		case "false":
			b = false
		default:
			sendToDiscord(ctx, "Unknown boolean.")
			return
		}

		err = roles.SetAllowGive(d, ctx.Msg.GuildID, r, b)
	case "set":
		r := routerRolesMatchRole(ctx)
		if r == nil {
			sendToDiscord(ctx, "No roles matched.")
			return
		}

		err = roles.SetString(d, ctx.Msg.GuildID, r, ctx.Args.Get(3))
	case "update":
		err = roles.Update(d, ctx.Msg.GuildID)
	case "reset":
		err = roles.ResetAll(d, ctx.Msg.GuildID)
	default:
		sendToDiscord(ctx, `Missing command:
	- split [role_name] (please update afterwards)
	- allow [role_name] [bool]
	- set [role_name] [custom_name]
	- update
	- reset`)

		return
	}

	if err != nil {
		sendToDiscord(ctx, err.Error())
		logger.Error(ctx.Ses, ctx.Msg.GuildID, err)

		return
	}

	sendToDiscord(ctx, "Done.")
}

func routerRolesList(ctx *exrouter.Context) {
	if ctx.Args.After(1) != "" {
		routerRole(ctx)
		return
	}

	regions, err := roles.GetRegions(ctx.Msg.GuildID)
	if err != nil || len(regions) == 0 {
		sendToDiscord(ctx, "Seems to be nothing here...")
		return
	}

	p := newPaginator(ctx)

	for _, rg := range regions {
		r, err := ctx.Ses.State.Role(ctx.Msg.GuildID, rg.StartID)
		if err != nil {
			logger.Error(ctx.Ses, ctx.Msg.GuildID, err)
			continue
		}

		var regionName = r.Name
		if rg.String != "" {
			regionName = rg.String
		}

		e := &discordgo.MessageEmbed{
			Color: 0x34be5b,
			Title: regionName,
		}

		for _, c := range rg.ChildrenIDs {
			r, err := ctx.Ses.State.Role(ctx.Msg.GuildID, c)
			if err != nil {
				logger.Error(ctx.Ses, ctx.Msg.GuildID, err)
				continue
			}

			e.Description += r.Name + "\n"
		}

		p.Add(e)
	}

	p.SetPageFooters()
	p.Spawn()
}

func routerRole(ctx *exrouter.Context) {
	role := routerRolesFindRole(ctx, ctx.Args.After(1))
	if role == nil {
		sendToDiscord(ctx, "Role not found.")
		return
	}

	e := &discordgo.MessageEmbed{
		Color: role.Color,
		Author: &discordgo.MessageEmbedAuthor{
			Name: role.Name,
		},
	}

	sendToDiscord(ctx, e)
}

func routerRolesAssign(ctx *exrouter.Context) {
	role := routerRolesFindRole(ctx, ctx.Args.After(1))
	if role == nil {
		sendToDiscord(ctx, "Role not found.")
		return
	}

	m, err := d.State.Member(ctx.Msg.GuildID, ctx.Msg.Author.ID)
	if err != nil {
		m, err = d.GuildMember(ctx.Msg.GuildID, ctx.Msg.Author.ID)
		if err != nil {
			sendToDiscord(ctx, err.Error())
			return
		}
	}

	for _, r := range m.Roles {
		if r == role.ID {
			err := d.GuildMemberRoleRemove(
				ctx.Msg.GuildID,
				ctx.Msg.Author.ID,
				role.ID,
			)

			if err != nil {
				sendToDiscord(ctx, "Failed to remove role!")
			} else {
				sendToDiscord(ctx, "Removed role for "+m.Mention())
			}

			return
		}
	}

	err = d.GuildMemberRoleAdd(
		ctx.Msg.GuildID,
		ctx.Msg.Author.ID,
		role.ID,
	)

	if err != nil {
		sendToDiscord(ctx, "Failed to add role!")
	} else {
		sendToDiscord(ctx, "Added role for "+m.Mention())
	}

	return
}

func routerRolesFindRole(ctx *exrouter.Context, match string) *discordgo.Role {
	match = strings.ToLower(match)

	regions, err := roles.GetRegions(ctx.Msg.GuildID)
	if err != nil || len(regions) == 0 {
		return nil
	}

	g, err := ctx.Ses.State.Guild(ctx.Msg.GuildID)
	if err != nil {
		return nil
	}

	var role *discordgo.Role

	for _, r := range g.Roles {
		if strings.ToLower(r.Name) == match {
			role = r
			break
		}
	}

	if role != nil {
		for _, rg := range regions {
			for _, c := range rg.ChildrenIDs {
				if c == role.ID {
					return role
				}
			}
		}
	}

	return nil
}

func routerRolesMatchRole(ctx *exrouter.Context) (r *discordgo.Role) {
	rls, err := ctx.Ses.GuildRoles(ctx.Msg.GuildID)
	if err != nil {
		errToDiscord(ctx, err)
		return
	}

	match := ctx.Args.Get(2)

	for _, rl := range rls {
		if rl.Name == match || rl.ID == match {
			r = rl
			break
		}
	}

	return
}
