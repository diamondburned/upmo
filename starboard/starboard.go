// Package starboard provides a plug-in handler for Discord starboard, using storm as its data store
package starboard

import (
	"github.com/asdine/storm"
	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
)

var (
	errUnitialized = errors.New("Fatal error: starboard unintialized with Initialize()")
)

// Settings contains settings for a guild's starboard
type Settings struct {
	GuildID string `storm:"unique,id"`

	WebhookID      string `storm:"unique"`
	WebhookToken   string `storm:"unique"`
	WebhookChannel string `stomr:"unique"`

	Starcount uint
	Reaction  string

	// Blacklisted channels
	Blacklist []string
}

// Message is a type for a starred message
type Message struct {
	ID        string `storm:"unique"`
	ChannelID string
	GuildID   string
}

// Setup sets up a starboard for a guild
func Setup(d *discordgo.Session, guildID, channelID string) error {
	g, err := d.State.Guild(guildID)
	if err != nil {
		return err
	}

	c, err := d.State.Channel(channelID)
	if err != nil {
		return err
	}

	node, err := node.Begin(true)
	if err != nil {
		return err
	}

	defer node.Rollback()

	var s Settings
	if err := node.One("GuildID", guildID, &s); err == nil {
		// Drop all past messages
		node.Drop(s.WebhookChannel)

		// Delete the old webhook
		d.WebhookDeleteWithToken(s.WebhookID, s.WebhookToken)
	}

	var webhookName = "Starboard hook: " + c.Name
	if len(webhookName) > 32 {
		webhookName = webhookName[:29] + "..."
	}

	w, err := d.WebhookCreate(c.ID, webhookName, "")
	if err != nil {
		return err
	}

	// Initializes the store for that guild
	node.From(c.ID).Init(&([]*Message{}))

	s = Settings{
		GuildID:        g.ID,
		WebhookID:      w.ID,
		WebhookToken:   w.Token,
		WebhookChannel: c.ID,
		Starcount:      4,
		Reaction:       "\u2b50", // ⭐
	}

	if err := node.Save(&s); err != nil {
		if err != storm.ErrAlreadyExists {
			return err
		}
	}

	return node.Commit()
}

func GetRxn(guildID string) (string, error) {
	var s Settings
	if err := node.One("GuildID", guildID, &s); err != nil {
		return "", errors.Wrap(err, "Can't find guild")
	}

	return s.Reaction, nil
}

// ChangeRxn changes the rune for the reaction
func ChangeRxn(d *discordgo.Session, guildID string, star string) error {
	return updateNode(func(node storm.Node) error {
		var s Settings
		if err := node.One("GuildID", guildID, &s); err != nil {
			return err
		}

		s.Reaction = star
		return node.Save(&s)
	})
}

// GetStarcount gets a guild's amount of stars needed to star.
func GetStarcount(guildID string) (uint, error) {
	var s Settings
	if err := node.One("GuildID", guildID, &s); err != nil {
		return 0, errors.Wrap(err, "Can't find guild")
	}

	return s.Starcount, nil
}

// ChangeStarcount updates the starcount for a guild
func ChangeStarcount(d *discordgo.Session, guildID string, count uint) error {
	return updateNode(func(node storm.Node) error {
		var s Settings
		if err := node.One("GuildID", guildID, &s); err != nil {
			return err
		}

		s.Starcount = count
		return node.Save(&s)
	})
}

func GetBlacklisted(guildID string) ([]string, error) {
	var s Settings
	if err := node.One("GuildID", guildID, &s); err != nil {
		return nil, errors.Wrap(err, "Can't find guild")
	}

	return s.Blacklist, nil
}

// ToggleBlacklist adds or removes a channel from the blacklist. True means
// added and false means removed.
func ToggleBlacklist(d *discordgo.Session, guildID, channelID string) (b bool, e error) {
	return b, updateNode(func(node storm.Node) error {
		var s Settings
		if err := node.One("GuildID", guildID, &s); err != nil {
			return err
		}

		for i, c := range s.Blacklist {
			if c == channelID {
				s.Blacklist = append(
					s.Blacklist[:i],
					s.Blacklist[i+1:]...,
				)

				b = false
				return node.Save(&s)
			}
		}

		b = true
		s.Blacklist = append(s.Blacklist, channelID)

		return node.Save(&s)
	})
}

// ResetMessage deletes the message from the store in case it bugged out
func ResetMessage(d *discordgo.Session, guildID, messageID string) error {
	var s Settings
	if err := node.One("GuildID", guildID, &s); err != nil {
		return errors.Wrap(err, "Error fetching guild")
	}

	err := updateNode(func(node storm.Node) error {
		node = node.From(s.WebhookChannel)
		if node == nil {
			return errors.New("Webhook channel empty, re-init sb")
		}

		return node.DeleteStruct(&Message{
			ID: messageID,
		})
	})

	if err != nil {
		return errors.Wrap(err, "Failed to delete message ID")
	}

	return nil
}

func updateNode(f func(node storm.Node) error) error {
	node, err := node.Begin(true)
	if err != nil {
		return err
	}

	defer node.Rollback()

	if err := f(node); err != nil {
		return err
	}

	return node.Commit()
}
