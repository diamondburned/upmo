package starboard

import (
	"fmt"
	"strings"
	"time"

	"github.com/asdine/storm"
	"github.com/asdine/storm/q"
	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/upmo/logger"
)

var everyoneEscaper = strings.NewReplacer(
	"@everyone", "@\u200be\u200bver\u200byo\u200bne",
	"@here", "@\u200bh\u200ber\u200be",
)

// Handler serves as a plug-in for the Discordgo Event Handler.
// This function handles everything in the database, easing out the library's
// usage. The function will return if Initialize() is not called.
func Handler(d *discordgo.Session, r *discordgo.MessageReactionAdd) {
	if err := handler(d, r); err != nil {
		logger.Error(d, r.GuildID, err)
	}
}

func handler(d *discordgo.Session, r *discordgo.MessageReactionAdd) error {
	if db == nil {
		return errUnitialized
	}

	// Get message from either the state cache or the channel
	m, err := d.State.Message(r.ChannelID, r.MessageID)
	if err != nil {
		m, err = d.ChannelMessage(r.ChannelID, r.MessageID)
		if err != nil {
			return err
		}
	}

	t, err := m.Timestamp.Parse()
	if err != nil {
		return err
	}

	// 48 hours == 2 days
	// Drop all messages older than 2 days
	if t.Add(time.Hour * 48).Before(time.Now()) {
		return nil
	}

	// If it's not in a guild, ignore it
	if r.GuildID == "" {
		return nil
	}

	var s Settings

	// Check if the guild is in the database
	if err := node.One("GuildID", r.GuildID, &s); err != nil {
		// Drop if it's not, it's not our business
		return nil
	}

	for _, c := range s.Blacklist {
		if c == r.ChannelID {
			// Channel is blacklisted, removing
			return nil
		}
	}

	// Support for wildcard emojis, aka any reaction.
	if s.Reaction == "*" || s.Reaction == r.Emoji.Name {
		// If the reaction is from the message author.
		if r.UserID == m.Author.ID {
			// we delete it. Seriously. Why would you do this? Only when we know
			// we don't accept any emojis though.
			if s.Reaction != "*" {
				// Error is not caught intentionally
				d.MessageReactionRemove(
					/* ChannelID */ r.ChannelID,
					/* MessageID */ r.MessageID,
					/* EmojiID */ s.Reaction,
					/* UserID */ m.Author.ID,
				)
			}

			return nil
		}
	}

	// We need this to calculate the number of emojis in the message. As
	// r.Emoji.Name is checked above to be equal to s.Reaction, we can use it.
	rxn := emojiInReactions(m.Reactions, r.Emoji.Name)
	if rxn == nil { // Emoji is not in the message
		return nil
	}

	// If the reaction count is not equal, drop it
	if rxn.Count < int(s.Starcount) {
		return nil
	}

	// Opens the bucket for this guild
	gdb, err := node.From(s.WebhookChannel).Begin(true)
	if err != nil {
		return err
	}

	defer gdb.Rollback()

	var mdb Message

	// Query the database for the message's ID
	if err := gdb.One("ID", r.MessageID, &mdb); err != storm.ErrNotFound {
		// If the error is NOT not found, drop it.
		// `err` is returned, since if the message is really in the
		// database, there would be no error (nil). Any other error
		// is fatal.
		return err
	}

	// We should try and garbage collect the old messages.
	q := gdb.Select(q.Lt(
		// If ID is less than a fake ID that would imply messages older than 2
		// days ago:
		"ID", timestampToMaxID(time.Now().Add(-time.Hour*48)),
	))

	// Not sure how the storm API works? It's been retarded since day 1.
	q.Delete(&Message{})

	// No need to commit the changes, we're doing it in the end.

	// By this point, we're certain that the message should be
	// starred, as it satisfies the conditions that the emoji
	// and the count match what we want.

	var nickname = m.Author.Username

	// Doing all this for a string of nickname
	if m, err := d.State.Member(r.GuildID, m.Author.ID); err == nil {
		if m.Nick != "" {
			nickname = m.Nick
		}
	}

	var embeds = make([]*discordgo.MessageEmbed, 0, len(m.Attachments))
	for _, a := range m.Attachments {
		if len(embeds) < 10 && a.Width > 0 && a.Height > 0 &&
			hasSuffices(a.URL, "png", "jpg", "jpeg", "gif") {

			embeds = append(embeds, &discordgo.MessageEmbed{
				Image: &discordgo.MessageEmbedImage{
					URL:      a.URL,
					ProxyURL: a.ProxyURL,
					Width:    a.Width,
					Height:   a.Height,
				},
			})

			continue
		}

		m.Content += "\n" + a.URL
	}

	// The message content, copied for mutating. We also need to escape
	// @everyone to prevent that certain incident.
	var content = everyoneEscaper.Replace(m.Content)

	// There are some cases where we need to do things to the content, such as
	// when the message is a join message.
	switch m.Type {
	case discordgo.MessageTypeGuildMemberJoin:
		content = m.Author.Mention() + " joined the server."
	case discordgo.MessageTypeChannelIconChange:
		content = m.Author.Mention() + " changed the channel icon."
	case discordgo.MessageTypeChannelNameChange:
		content = m.Author.Mention() + " changed the channel name to " +
			content + "."
	case discordgo.MessageTypeChannelPinnedMessage:
		content = m.Author.Mention() + " pinned message " + m.ID + "."
	}

	// We prepend an emoji at the start, if the guild accepts any reaction.
	if s.Reaction == "*" {
		var emoji string

		if r.Emoji.ID == "" {
			emoji = r.Emoji.Name
		} else {
			// Try and see if the emoji is in the guild we're in
			g, _ := getGuild(d, r.GuildID)
			if e := emojiInGuild(g, r.Emoji.ID); e != nil {
				// Emoji is in the guild, we're gonna use it.
				emoji = r.Emoji.MessageFormat()
			} else {
				// Emoji isn't, we can't do much here.
				emoji = fmt.Sprintf("[emoji](%s)", emojiURL(r.Emoji))
			}
		}

		content = emoji + " | " + content
	}

	webhook := &discordgo.WebhookParams{
		Username: nickname,
		Content: fmt.Sprintf(
			// message (source)
			"%s [*(source)*](<https://discordapp.com/channels/%s/%s/%s>)",
			content, r.GuildID, r.ChannelID, r.MessageID,
		),
		AvatarURL: m.Author.AvatarURL("512"),
		Embeds:    embeds,
	}

	if err := d.WebhookExecute(s.WebhookID, s.WebhookToken, true, webhook); err != nil {
		return err
	}

	// Save the message, only after everything went fine.
	mdb = Message{
		ID:        r.MessageID,
		ChannelID: r.ChannelID,
		GuildID:   r.GuildID,
	}

	// Save it into the database
	if err := gdb.Save(&mdb); err != nil {
		// This shouldn't happen, unless the error is an actual error.
		return errors.Wrap(err, "Failed to save the message to the database")
	}

	return gdb.Commit()
}

func hasSuffices(str string, suffices ...string) bool {
	str = strings.ToLower(str)

	for _, suffix := range suffices {
		if strings.HasSuffix(str, suffix) {
			return true
		}
	}

	return false
}
