package starboard

import (
	"strconv"
	"time"

	"github.com/bwmarrin/discordgo"
)

func emojiInReactions(rs []*discordgo.MessageReactions, rxn string) *discordgo.MessageReactions {
	for _, r := range rs {
		if r.Emoji != nil && r.Emoji.Name == rxn {
			return r
		}
	}

	return nil
}

func getGuild(s *discordgo.Session, gID string) (*discordgo.Guild, error) {
	g, err := s.State.Guild(gID)
	if err != nil {
		g, err = s.Guild(gID)
	}

	if err != nil {
		return nil, err
	}

	return g, nil
}

const (
	// 5 Fs makes 5*(1111) or 20 1s. 3 is 0011, which bumps it to 22.
	last22bits = 0x3FFFFF
	msInNano   = 1000000
	ms2015     = 1420070400000
)

func timestampToMaxID(time time.Time) string {
	// Create a snowflake that occupies the last 22 bits, as a ceiling ID.
	var snowflake = int64(last22bits)

	// Convert the given time to milliseconds since 2015.
	var ms = time.UnixNano()/msInNano - ms2015

	// Add the milliseconds to the timestamp.
	snowflake |= ms << 22

	return strconv.FormatInt(snowflake, 10)
}

func emojiInGuild(g *discordgo.Guild, emojiID string) *discordgo.Emoji {
	if g == nil {
		return nil
	}

	for _, e := range g.Emojis {
		if e.ID == emojiID {
			return e
		}
	}

	return nil
}

func emojiURL(e discordgo.Emoji) string {
	base := "https://cdn.discordapp.com/emojis/" + e.ID
	if e.Animated {
		return base + ".gif"
	} else {
		return base + ".png"
	}
}
