package latex

import (
	"gitlab.com/diamondburned/upmo/playground"
)

func init() {
	playground.AddPlayground("math", func(header, body string) (*playground.Result, error) {
		return exec(header, "$$"+body+"$$")
	})
}
