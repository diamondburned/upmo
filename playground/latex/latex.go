package latex

import (
	"bytes"
	"encoding/json"
	"gitlab.com/diamondburned/upmo/playground"
	"net/http"
	"strings"

	"io/ioutil"

	"gitlab.com/diamondburned/upmo/playground/client"
)

const (
	// Compile is the constant to the compile URL
	Compile = "http://rtex.probablyaweb.site/api/v2"
)

func init() {
	playground.AddPlayground("latex", exec)
	playground.AddPlayground("tex", exec)
}

type latexDefaults struct {
	Code   string `json:"code"`
	Format string `json:"format"`
}

type latex struct {
	Status      string `json:"status"`
	Log         string `json:"log"`
	Description string `json:"description"`
	Filename    string `json:"filename"`
}

func exec(header, body string) (*playground.Result, error) {
	var l = &latex{}
	var err error

	if body == "" {
		err = l.Execute(header)
	} else {
		err = l.ExecuteShorthand(header, body)
	}

	if err != nil {
		return nil, err
	}

	return &playground.Result{
		Output: "",
		Errors: l.GetErrors(),
		Image:  l.GetImage(),
	}, nil
}

// ExecuteShorthand executes the code with boilerplate code
func (l *latex) ExecuteShorthand(prefix, input string) error {
	code := strings.ReplaceAll(latexTemplate, "#CONTENT", input)

	if prefix != "" {
		code = strings.ReplaceAll(code, "#PREFIX", prefix)
	} else {
		code = strings.ReplaceAll(code, "#PREFIX", "")
	}

	return l.Execute(code)
}

// Execute runs the code in the playground
func (l *latex) Execute(input string) error {
	s, err := json.Marshal(&latexDefaults{
		Format: "png",
		Code:   input,
	})

	if err != nil {
		return err
	}

	req, err := client.Post(Compile, bytes.NewReader(s))
	if err != nil {
		return err
	}

	defer req.Body.Close()

	return json.NewDecoder(req.Body).Decode(&l)
}

func (l *latex) GetImage() []byte {
	res, err := http.Get(Compile + "/" + l.Filename)

	if err != nil {
		return nil
	}

	defer res.Body.Close()

	img, err := ioutil.ReadAll(res.Body)

	if err != nil {
		return nil
	}

	return img
}

func (l *latex) GetErrors() string {
	return l.Description
}
