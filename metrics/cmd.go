package metrics

import (
	"strings"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/upmo/logger"
)

func DropMetrics(guildID, optionalKey string) {
	state.mut.Lock()
	defer state.mut.Unlock()

	if optionalKey == "" {
		delete(state.metrics, guildID)
		return
	}

	gm, ok := state.metrics[guildID]
	if !ok {
		return
	}

	delete(gm, strings.TrimPrefix(optionalKey, "guild:"))
}

func TrackChannel(guildID string, ch *discordgo.Channel) {
	increase(guildID, "channel:"+ch.Name, 0, defaultCounter("messages"))
}

func DropMetCommand(ctx *exrouter.Context) {
	DropMetrics(ctx.Msg.GuildID, ctx.Args.After(1))
	ctx.Reply("Done")
}

func TrackChCommand(ctx *exrouter.Context) {
	c, err := ctx.Channel(ctx.Msg.ChannelID)
	if err != nil {
		logger.Error(ctx.Ses, ctx.Msg.GuildID,
			errors.Wrap(err, "Failed to get channel"))
		return
	}

	TrackChannel(ctx.Msg.GuildID, c)
	ctx.Reply("Done")
}
