package metrics

import (
	"context"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"gitlab.com/shihoya-inc/errchi"
)

func CheckOAuth(next errchi.Handler) errchi.Handler {
	return errchi.HandlerFunc(func(w http.ResponseWriter, r *http.Request) (int, error) {
		// Don't care if no OAuth info is given
		if OAuthSettings.ClientSecret == "" {
			return next.ServeHTTP(w, r)
		}

		// Get the cookie
		c, err := r.Cookie("access-token")
		if err != nil {
			http.Redirect(w, r, GetOAuthURL(), 302)
			return 0, nil
		}

		// Check
		knownUsers.mu.Lock()
		gIDs, ok := knownUsers.guildIDs[c.Value]
		knownUsers.mu.Unlock()

		if !ok {
			http.Redirect(w, r, GetOAuthURL(), 302)
			return 0, nil
		}

		var targetID = chi.URLParam(r, "guildID")
		if targetID == "" {
			return 404, errors.New("guild not found")
		}

		for _, gID := range gIDs {
			if gID == targetID {
				return next.ServeHTTP(w, r)
			}
		}

		return 404, errors.New("guild not found")
	})
}

func requestCtxValue(r *http.Request, k string, v interface{}) *http.Request {
	return r.WithContext(context.WithValue(r.Context(), k, v))
}
