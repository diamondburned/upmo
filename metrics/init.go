package metrics

import (
	"encoding/json"
	"net/http"
	"strings"

	"github.com/asdine/storm"
	"github.com/bwmarrin/discordgo"
	"github.com/diamondburned/metric"
	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/upmo/webserver"
	"gitlab.com/shihoya-inc/errchi"
	"go.etcd.io/bbolt"
)

func Initialize(s *discordgo.Session, db *storm.DB) error {
	if err := ApplyMetricSettings(); err != nil {
		return err
	}

	state.session = s

	state.acquire = func(with func(*bbolt.Bucket) error) error {
		return db.Bolt.Update(func(tx *bbolt.Tx) error {
			// tx.DeleteBucket([]byte("metrics_v2"))

			b, err := tx.CreateBucketIfNotExists([]byte("metrics_v2"))
			if err != nil {
				return errors.Wrap(err, "Failed to create metrics bucket")
			}

			if err := with(b); err != nil {
				return err
			}

			return nil
		})
	}
	state.view = func(with func(*bbolt.Bucket) error) error {
		return db.Bolt.View(func(tx *bbolt.Tx) error {
			b := tx.Bucket([]byte("metrics_v2"))
			if b == nil {
				return errors.New("Can't find metrics_v2 bucket")
			}

			if err := with(b); err != nil {
				return err
			}

			return nil
		})
	}

	// Hook
	s.AddHandler(func(_ *discordgo.Session, m *discordgo.MessageCreate) {
		if m.GuildID == "" {
			return
		}

		increase(m.GuildID, "messages", 1, defaultCounter("messages"))

		c, err := s.State.Channel(m.ChannelID)
		if err != nil {
			c, err = s.Channel(m.ChannelID)
		}
		if err != nil {
			return
		}

		increase(m.GuildID, "channel:"+c.Name, 1, nil)
	})

	s.AddHandler(func(_ *discordgo.Session, m *discordgo.GuildMemberAdd) {
		increase(m.GuildID, "member delta", 1, defaultCounter("members"))
	})

	s.AddHandler(func(_ *discordgo.Session, m *discordgo.GuildMemberRemove) {
		increase(m.GuildID, "member delta", -1, defaultCounter("members"))
	})

	// Make sure the bucket exists
	err := state.acquire(func(*bbolt.Bucket) error { return nil })
	if err != nil {
		return errors.Wrap(err, "Failed to create the bucket")
	}

	if err := load(); err != nil {
		return errors.Wrap(err, "Failed to load")
	}

	cancellables = append(cancellables, eventLoop(s))
	return nil
}

var cancellables []func()

func Stop() {
	for _, fn := range cancellables {
		fn()
	}
}

const route = "/metrics"

func init() {
	state.metrics = map[string]map[string]metric.Metric{}

	webserver.Route(route, func(r *errchi.Router) {
		// Endpoint to sign in using OAuth
		r.Handle("/register", errchi.HandlerFunc(OAuthHandler))

		// Access endpoint
		r.Route("/{guildID:[0-9]+}", func(r *errchi.Router) {
			r.Use(CheckOAuth)

			// Render the metrics page
			r.Get("/", serveWithGlobal(false))
			// Get the json
			r.Get("/json", serveWithGlobal(true))
		})

		// Anything else basically 404
	})
}

func serveWithGlobal(jsonOutput bool) errchi.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (int, error) {
		key := chi.URLParam(r, "guildID")

		state.mut.Lock()
		defer state.mut.Unlock()

		guildMet, ok := state.metrics[key]
		if !ok {
			return 404, errors.New("guild not found")
		}

		global := state.metrics["global"]

		exposed := make(map[string]metric.Metric,
			len(guildMet)+len(global))

		for k, v := range guildMet {
			if !strings.HasPrefix(k, "channel:") {
				k = "guild:" + k
			}

			exposed[k] = v
		}

		for k, v := range global {
			exposed["global:"+k] = v
		}

		if jsonOutput {
			return 0, json.NewEncoder(w).Encode(exposed)
		}

		h := metric.Handler(func() map[string]metric.Metric { return exposed })
		h.ServeHTTP(w, r)

		return 0, nil
	}
}
