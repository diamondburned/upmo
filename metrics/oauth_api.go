package metrics

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
)

type OAuth struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	ExpiresIn    int64  `json:"expires_in"` // elapsed seconds
	RefreshToken string `json:"refresh_token"`
	Scope        string `json:"scope"`
}

func (oa OAuth) Do(r *http.Request) (*http.Response, error) {
	r.Header.Set("Authorization", "Bearer "+oa.AccessToken)
	return client.Do(r)
}

func (oa OAuth) Get(url string) (*http.Response, error) {
	r, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to create a request")
	}

	return oa.Do(r)
}

func (oa OAuth) Identify() (*discordgo.User, error) {
	r, err := oa.Get(discordgo.EndpointUser("@me"))
	if err != nil {
		return nil, errors.Wrap(err, "Failed to connect to Discord")
	}

	defer r.Body.Close()

	var user discordgo.User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		// Log to console since it's sensitive
		log.Println("Failed to decode Discord's JSON:", err)

		return nil, errors.New("Failed to decode Discord's user response")
	}

	return &user, nil
}

func (oa OAuth) GetGuildIDs() ([]string, error) {
	r, err := oa.Get(discordgo.EndpointUserGuilds("@me"))
	if err != nil {
		return nil, errors.Wrap(err, "Failed to connect to Discord")
	}

	defer r.Body.Close()

	var guilds []struct {
		ID string `json:"id"`
	}

	if err := json.NewDecoder(r.Body).Decode(&guilds); err != nil {
		// Log to console since it's sensitive
		log.Println("Failed to decode Discord's JSON:", err)

		return nil, errors.New("Failed to decode Discord's guilds response")
	}

	var ids = make([]string, len(guilds)) // really can't just map this, go pls
	for i, g := range guilds {
		ids[i] = g.ID
	}

	return ids, nil
}
