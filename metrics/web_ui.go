package metrics

import (
	"fmt"
	"html/template"
	"strings"
	"time"

	"github.com/diamondburned/metric"
	humanize "github.com/dustin/go-humanize"
)

func ApplyMetricSettings() error {
	metric.DefaultSorter = func(i, j string) bool {
		switch {
		case cmpSamePrefix(i, j, "channel:"),
			cmpSamePrefix(i, j, "guild:"),
			cmpSamePrefix(i, j, "global:"):

			return strings.Compare(i, j) < 0
		}

		// Global > guild > channel#

		return cmpPrefix(i, "global:", j, "guild:") ||
			cmpPrefix(i, "guild:", j, "channel:")
	}

	return metric.ReplaceHTML(HTMLTemplate, customFns)
}

func cmpPrefix(i, p1, j, p2 string) bool {
	return strings.HasPrefix(i, p1) && strings.HasPrefix(j, p2)
}

func cmpSamePrefix(i, j, prefix string) bool {
	return cmpPrefix(i, prefix, j, prefix)
}

var customFns = template.FuncMap{
	"fmt": func(input float64, unit string) string {
		if unit != "ms" {
			return humanize.FtoaWithDigits(input, 10) + " " + unit
		}

		// How many $1 are in a $2?
		const (
			msSec  = float64(time.Second / time.Millisecond)
			msMin  = float64(time.Minute / time.Millisecond)
			msHour = float64(time.Hour / time.Millisecond)
		)

		switch {
		case input > msHour:
			return fmt.Sprintf("%.0f hours", input/msHour)
		case input > msMin:
			return fmt.Sprintf("%.0f minutes", input/msMin)
		case input > msSec:
			return fmt.Sprintf("%.4f seconds", input/msSec)
		default:
			return humanize.Ftoa(input) + " ms"
		}
	},
}

const HTMLTemplate = `
<!DOCTYPE html>
<html lang="us">

<head>
	<meta charset="utf-8">
	<title>upmo metrics</title>
	<meta name="viewport" content="width=device-width">
	<!-- <meta http-equiv="refresh" content="5"> -->
	
	<style>
	* { 
		margin: 0; 
		padding: 0; 
		box-sizing: border-box; 
		font-family: monospace; 
		font-size: 12px;
	}

	.container {
	    max-width: 1000px;
	    margin: 0 auto;
	    display: flex;
	    flex-direction: column;
	    padding: 25px;
	    border-left: 1px grey solid;
	    border-right: 1px grey solid;
		min-height: 100vh;
	}

	h1 { text-align: center; }
	h2 {
		font-weight: normal;
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;
	}
	h2.col-1 {
		font-size: 1.2em;
	}
	p > b { white-space: pre; }
	.metric {
		padding: 1em 0;
		border-top: 1px solid rgba(0,0,0,0.33);
	}

	.row {
		display: flex;
		flex-direction: row;
		align-items: center;
		margin: 2px 0;
	}
	.col-1 { flex: 1; }
	.col-2 { flex: 3; }

	.timeline { width: 5em; }

	path { 
		fill: none; 
		stroke: rgba(0,0,0,0.33);
		stroke-width: 1; 
		stroke-linecap: round; 
		stroke-linejoin: round;
	}
	path:last-child { 
		stroke: black;
	}

	@media screen and (max-width: 700px) {
		.row.graph > svg { width: 70vw; }
		.row             { flex-direction: column; }
		h2.col-1         { padding-bottom: 10px; }
	}
	</style>
</head>

<body>
	<div class="container">
	<div><h1><pre>    __          __
.--------..-----.|  |_ .----.|__|.----..-----.
|        ||  -__||   _||   _||  ||  __||__ --|
|__|__|__||_____||____||__|  |__||____||_____|
	</pre></h1></div>
	{{ range . }}
		<div class="row metric">
		  <h2 class="col-1">{{ .name }}</h2>
			<div class="col-2">
			{{ if .type }}
				<div class="row">
					{{ template "table" . }}
					<div class="col-1"></div>
				</div>
			{{ else if .interval }}
				<div class="row">{{ template "timeseries" . }}</div>
			{{ else if .metrics}}
				{{ range .metrics }}
					<div class="row">
					{{ template "timeseries" . }}
					</div>
				{{ end }}
			{{ end }}
			</div>
	  </div>
	{{ end }}
</div>
</body>

</html>

{{ define "table" }}
	<div class="col-1">
	{{ if eq .type "c" }}
		<p><b>amount</b> {{fmt .count .unit}}</p>
	{{ else if eq .type "g" }}
		<p><b>current</b> {{fmt .value .unit}}</p>
		<p><b>min    </b> {{fmt .min .unit}}</p>
		<p><b>max    </b> {{fmt .max .unit}}</p>
	{{ else if eq .type "h" }}
		<p><b>50th percentile</b> {{fmt .p50 .unit}}</p>
		<p><b>90th percentile</b> {{fmt .p90 .unit}}</p>
		<p><b>99th percentile</b> {{fmt .p99 .unit}}</p>
	{{ end }}
	</div>
{{ end }}

{{ define "timeseries" }}
  	{{ template "table" .total }}

	<div class="col-1">
		<div class="row graph">
			<div class="timeline">{{ duration .samples .interval }}</div>
			<svg class="col-1" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 20">
			{{ if eq (index (index .samples 0) "type") "c" }}
				{{ range (path .samples "count") }}<path d={{ . }} />{{end}}
			{{ else if eq (index (index .samples 0) "type") "g" }}
				{{ range (path .samples "value" "min" "max" ) }}<path d={{ . }} />{{end}}
			{{ else if eq (index (index .samples 0) "type") "h" }}
				{{ range (path .samples "p50" "p90" "p99") }}<path d={{ . }} />{{end}}
			{{ end }}
			</svg>
		</div>
	</div>
{{ end }}
`
