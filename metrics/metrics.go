package metrics

import (
	"encoding/json"
	"log"
	"sync"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/diamondburned/metric"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/clocker"
	"go.etcd.io/bbolt"
)

// guildID
var state struct {
	metrics map[string]map[string]metric.Metric
	mut     sync.Mutex

	session *discordgo.Session

	acquire func(withTx func(*bbolt.Bucket) error) error
	view    func(withTx func(*bbolt.Bucket) error) error
	// db      *bbolt.DB
}

func save() error {
	if len(state.metrics) < 1 {
		return nil
	}

	return state.acquire(func(b *bbolt.Bucket) error {
		state.mut.Lock()
		defer state.mut.Unlock()

		for guildID, metrics := range state.metrics {
			if guildID == "global" {
				continue
			}

			g, err := b.CreateBucketIfNotExists([]byte(guildID))
			if err != nil {
				return errors.Wrap(err, "Failed for guild "+guildID)
			}

			for name, metric := range metrics {
				j, err := json.Marshal(metric)
				if err != nil {
					return errors.Wrap(err,
						"Failed to marshal metric for guild "+guildID)
				}

				if err := g.Put([]byte(name), j); err != nil {
					return errors.Wrap(err,
						"Failed to put metric "+name+" of guild ID "+guildID)
				}
			}
		}

		if _, ok := state.metrics["global"]; !ok {
			state.metrics["global"] = map[string]metric.Metric{}
		}

		return nil
	})
}

func load() error {
	return state.view(func(b *bbolt.Bucket) error {
		state.mut.Lock()
		defer state.mut.Unlock()

		return b.ForEach(func(name, v []byte) error {
			if len(v) > 0 {
				return nil
			}

			if string(name) == "global" {
				return nil
			}

			guildMap, ok := state.metrics[string(name)]
			if !ok {
				guildMap = map[string]metric.Metric{}
				state.metrics[string(name)] = guildMap
			}

			// Definitely a bucket
			return b.Bucket(name).ForEach(func(name, metJSON []byte) error {
				m, err := metric.LoadMetricJSON(metJSON)
				if err != nil {
					return errors.Wrap(err,
						"Failed to parse metrics for guildID "+string(name))
				}

				guildMap[string(name)] = m
				return nil
			})
		})
	})
}

func eventLoop(s *discordgo.Session) func() {
	stop := make(chan struct{})

	go func() {
		tick := clocker.NewTicker(5 * time.Second)

		for {
			// Add the Discord latency in
			global(
				"latency",
				float64(state.session.HeartbeatLatency().Milliseconds()),
				defaultGauge("ms"),
			)

			if err := save(); err != nil {
				log.Println("ERROR saving metrics:", err)
			}

			select {
			case <-stop:
				return
			case _ = <-tick.C:
				continue
			}
		}
	}()

	return func() { stop <- struct{}{} }
}

func runEvery(t time.Time, every time.Duration, fn func()) {
	// TODO: test
	if t.Round(time.Second).Equal(t.Truncate(every).Add(every)) {
		fn()
	}
}

type constructor func() metric.Metric

var (
	// 15m-15s: 15 minutes, 15 seconds each, roughly 60 points
	//  1d-30m: 1 day, 30 minutes each, roughly 48 points
	//  1M-12h: 1 month, 12 hours each, roughly 60 points

	defaultHistogram = func(unit string) constructor {
		return func() metric.Metric {
			return metric.NewHistogram(unit, "15m15s", "1d30m", "1M12h")
		}
	}
	defaultCounter = func(unit string) constructor {
		return func() metric.Metric {
			return metric.NewCounter(unit, "15m15s", "1d30m", "1M12h")
		}
	}
	defaultGauge = func(unit string) constructor {
		return func() metric.Metric {
			return metric.NewGauge(unit, "15m15s", "1d30m", "1M12h")
		}
	}
)

func global(key string, incr float64, newMet constructor) {
	increase("global", key, incr, newMet)
}

func increase(guildID string, key string, incr float64, newMet constructor) {
	if guildID == "" || key == "" {
		return
	}

	state.mut.Lock()
	defer state.mut.Unlock()

	gm, ok := state.metrics[guildID]
	if !ok {
		gm = map[string]metric.Metric{}
		state.metrics[guildID] = gm
	}

	mt, ok := gm[key]
	if !ok {
		if newMet == nil {
			return
		}

		mt = newMet()
		gm[key] = mt
	}

	mt.Add(incr)
}
