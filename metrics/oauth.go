package metrics

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"sync"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/gorilla/schema"
	"github.com/pkg/errors"
)

const Scopes = "guilds"

// Use Discord OAuth to check for guilds

var knownUsers struct {
	// pairs oauth token with guild ids
	guildIDs map[string][]string
	mu       sync.Mutex
}

var OAuthSettings struct {
	ClientID     string
	ClientSecret string
	// RedirectURI  string // https://upmo.diamondb.xyz
	Host string // https://upmo.diamondb.xyz [/metrics/:guildID]
}

func GetRedirectURI() string {
	return OAuthSettings.Host + "/metrics/register"
}

var decoder = schema.NewDecoder()

var client = http.Client{
	Timeout: 2 * time.Second,
}

func GetOAuthURL() string {
	values := url.Values{}
	values.Set("client_id", OAuthSettings.ClientID)
	values.Set("redirect_uri", GetRedirectURI())
	values.Set("response_type", "code")
	values.Set("scope", Scopes)

	return "https://discordapp.com/api/oauth2/authorize?" + values.Encode()
}

func RequestTokenURL(code string) string {
	values := url.Values{}
	values.Set("client_id", OAuthSettings.ClientID)
	values.Set("client_secret", OAuthSettings.ClientSecret)
	values.Set("grant_type", "authorization_code")
	values.Set("code", code)
	values.Set("redirect_uri", GetRedirectURI())
	values.Set("scope", Scopes)

	return discordgo.EndpointOauth2 + "token"
}

// OAuthHandler handles incoming requests from Discord with a code in the URL
// form
func OAuthHandler(w http.ResponseWriter, r *http.Request) (int, error) {
	if err := r.ParseForm(); err != nil {
		return 400, errors.Wrap(err, "Failed to parse form")
	}

	var OAResp struct {
		Code string `schema:"code,required"`
		// State string `schema:"state"`
	}

	if err := decoder.Decode(&OAResp, r.Form); err != nil {
		return 400, errors.Wrap(err, "Invalid form")
	}

	var now = time.Now()

	// POST to Discord
	oresp, err := client.Post(RequestTokenURL(OAResp.Code),
		"application/x-www-form-urlencoded", nil)
	if c, err := mustRequest(oresp, err); err != nil {
		return c, err
	}

	var oa OAuth
	if c, err := mustUnmarshal(oresp, &oa); err != nil {
		return c, err
	}

	/*
		// Get the user's info
		u, err := oa.Identify()
		if err != nil {
			wError(w, err)
			return
		}
	*/

	// Get the user's guilds
	gIDs, err := oa.GetGuildIDs()
	if err != nil {
		return 500, errors.Wrap(err, "Failed to get guild IDs")
	}

	knownUsers.mu.Lock()

	// Set the user's known guilds
	knownUsers.guildIDs[oa.AccessToken] = gIDs

	knownUsers.mu.Unlock()

	// Set the cookie
	http.SetCookie(w, &http.Cookie{
		Name:    "access-token",
		Value:   oa.AccessToken,
		Expires: now.Add(time.Duration(oa.ExpiresIn) * time.Second),
	})

	// Check referer, redirect if needed
	if referer := r.Header.Get("Referer"); referer != "" {
		// Here we use HTTP 303 Found, as we don't want the user to hit Back and
		// resubmit.
		http.Redirect(w, r, referer, 303)
		return 0, nil
	}

	return 200, nil
}

// defer body close after this passes
func mustRequest(resp *http.Response, err error) (int, error) {
	if err != nil {
		resp.Body.Close()
		return 500, errors.Wrap(err, "Failed to connect to Discord")
	}

	if resp.StatusCode != 200 {
		// Read the error and log
		b, _ := ioutil.ReadAll(resp.Body)
		log.Println("Discord rejected, response:", string(b))

		resp.Body.Close()
		return 500, errors.New("Discord rejected")
	}

	return 200, nil
}

func mustUnmarshal(resp *http.Response, v interface{}) (int, error) {
	defer resp.Body.Close()

	if err := json.NewDecoder(resp.Body).Decode(v); err != nil {
		// Log to console since it's sensitive
		log.Println("Failed to decode Discord's JSON:", err)

		return 500, errors.New("Failed to decode Discord's response")
	}

	return 200, nil
}

func wError(w http.ResponseWriter, err error) {
	w.WriteHeader(400)
	w.Write([]byte(err.Error()))
}
