#!/usr/bin/env bash
declare -A org
for f in output/*; {
	[[ -d "$f" ]] && continue
	[[ "$f" != *".png" ]] && continue

	# "${f%%-*}" == "output/menhara"
	org["${f%%-*}"]+="$f"$'\n'
}

for d in "${!org[@]}"; {
	echo "Making directory $d"
	mkdir -p "$d/"

	IFS=$'\n' read -d '' -ra files <<< "${org[$d]}"

	echo "Moving files..."
	# Iterate over files in the associative array
	mv "${files[@]}" "$d/"
}
