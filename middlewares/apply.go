package middlewares

import "github.com/Necroforger/dgrouter/exrouter"

func With(ctx *exrouter.Context, callback func(ctx *exrouter.Context),
	middlewares ...exrouter.MiddlewareFunc) {

	Apply(callback, middlewares...)(ctx)
}

func Apply(callback func(ctx *exrouter.Context),
	middlewares ...exrouter.MiddlewareFunc) func(ctx *exrouter.Context) {

	for _, middleware := range middlewares {
		callback = middleware(callback)
	}

	return callback
}
