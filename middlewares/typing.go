package middlewares

import (
	"time"

	"github.com/Necroforger/dgrouter/exrouter"
)

var TypingTimeout = 60 * time.Second

// Typing triggers a Typing event
func Typing(h exrouter.HandlerFunc) exrouter.HandlerFunc {
	return func(ctx *exrouter.Context) {
		done := make(chan struct{})
		go func() {
			h(ctx)
			done <- struct{}{}
		}()

		expire := time.After(TypingTimeout)
		var stop bool

		for {
			if !stop {
				// Stop sending typing requests if you can't send it anyway
				stop = ctx.Ses.ChannelTyping(ctx.Msg.ChannelID) != nil
			}

			select {
			case <-time.After(8 * time.Second):
				continue // keep sending typing requests
			case <-expire:
				<-done // Wait for the goroutine to finish
				return // Exit
			case <-done:
				return // Exit
			}
		}
	}
}
