package tags

import (
	"strconv"
	"time"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/Necroforger/dgwidgets"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/upmo/logger"
	"gitlab.com/diamondburned/upmo/utils"
)

func lookup(ctx *exrouter.Context) {
	g, err := ctx.Guild(ctx.Msg.GuildID)
	if err != nil {
		ctx.Reply("Error fetching guild: " + err.Error())
		return
	}

	tags, err := SearchTags(g, ctx.Args.After(1))
	if err != nil {
		ctx.Reply("Error: " + err.Error())
		return
	}

	p := dgwidgets.NewPaginator(ctx.Ses, ctx.Msg.ChannelID)
	p.Loop = false
	p.DeleteReactionsWhenDone = true
	p.ColourWhenDone = 0xFF0000
	p.Widget.Timeout = time.Second * 30
	p.Widget.UserWhitelist = []string{
		ctx.Msg.Author.ID,
	}

	var (
		embeds = make([]*discordgo.MessageEmbed, 0, len(tags)/10+1)
		title  = strconv.Itoa(len(tags)) + " tags found"
	)

	for i := 0; i < len(tags); i += 10 {
		embed := &discordgo.MessageEmbed{
			Title: title,
			Color: 0x2e5ed2,
		}

		for j := i; j-i < 10 && j < len(tags); j++ {
			embed.Description += "`" + utils.Sanitize(tags[j].Name) + "`\n"
		}

		embeds = append(embeds, embed)
	}

	p.Add(embeds...)
	p.SetPageFooters()

	if err := p.Spawn(); err != nil {
		logger.Error(ctx.Ses, ctx.Msg.GuildID, err)
	}
}
