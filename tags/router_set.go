package tags

import (
	"fmt"
	"time"

	"github.com/Necroforger/dgrouter/exrouter"
)

func set(ctx *exrouter.Context) {
	g, err := ctx.Guild(ctx.Msg.GuildID)
	if err != nil {
		ctx.Reply("Error fetching guild: " + err.Error())
		return
	}

	t, err := GetTag(g, ctx.Args.Get(1))
	if err == nil {
		b, err := tagUserAllowed(ctx, t)
		if err != nil {
			ctx.Reply("Error: " + err.Error())
			return
		}

		if !b {
			ctx.Reply("Tag is not yours.")
			return
		}
	}

	t = &Tag{
		Name:    ctx.Args.Get(1),
		Content: ctx.Args.After(2),
		Attachments: make(
			[]string, 0, len(ctx.Msg.Attachments)+len(ctx.Msg.Embeds),
		),
		Author:    ctx.Msg.Author.ID,
		Timestamp: time.Now(),
	}

	for _, a := range ctx.Msg.Attachments {
		t.Attachments = append(t.Attachments, a.URL)
	}

	for _, e := range ctx.Msg.Embeds {
		if e.Thumbnail != nil && e.Thumbnail.Width > 0 {
			t.Attachments = append(t.Attachments, e.Thumbnail.URL)
		}
	}

	if t.Content == "" && len(t.Attachments) == 0 {
		ctx.Reply("Error: missing content")
		return
	}

	if err := AddTag(g, t); err != nil {
		ctx.Reply("Error: " + err.Error())
		return
	}

	ctx.Reply(fmt.Sprintf(
		"Set tag %s of user <@%s>.",
		t.Name, t.Author,
	))
}
