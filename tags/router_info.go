package tags

import (
	"time"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/upmo/logger"
)

func info(ctx *exrouter.Context) {
	g, err := ctx.Guild(ctx.Msg.GuildID)
	if err != nil {
		ctx.Reply("Error fetching guild: " + err.Error())
		return
	}

	t, err := GetTag(g, ctx.Args.After(1))
	if err != nil {
		ctx.Reply("Error: " + err.Error())
		return
	}

	var displayName string

	m, err := ctx.Member(g.ID, t.Author)
	if err != nil {
		displayName = t.Author
	} else {
		displayName = m.User.Username + "#" + m.User.Discriminator
		if m.Nick != "" {
			displayName += " (" + m.Nick + ")"
		}

	}

	embed := &discordgo.MessageEmbed{
		Title:       t.Name,
		Description: t.Content,
		Author: &discordgo.MessageEmbedAuthor{
			Name:    displayName,
			IconURL: m.User.AvatarURL("32"),
		},
		Timestamp: t.Timestamp.Format(time.RFC3339),
	}

	if len(t.Attachments) > 0 {
		embed.Image = &discordgo.MessageEmbedImage{
			URL: t.Attachments[0],
		}

		for _, a := range t.Attachments {
			embed.Description += a + "\n"
		}
	}

	_, err = ctx.Ses.ChannelMessageSendEmbed(ctx.Msg.ChannelID, embed)
	if err != nil {
		logger.Error(ctx.Ses, ctx.Msg.GuildID, err)
	}
}
