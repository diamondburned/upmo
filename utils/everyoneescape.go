package utils

import (
	"unicode"
)

// EscapeEveryone escapes @everyone and @here mentions.
func EscapeEveryone(s string) string {
	var mentionBuf string
	var mentionInd int

	for i, r := range s {
		if r == '@' && i != len(s)-1 {
			mentionBuf += string(r)
			mentionInd = i
			continue
		}

		if unicode.IsSpace(r) {
			mentionBuf = ""
			continue
		}

		if mentionBuf != "" && unicode.IsLetter(r) {
			mentionBuf += string(r)
		}

		if stringsCmp(mentionBuf, "@everyone", "@here") {
			s = s[:mentionInd+1] + "\u200b" + s[mentionInd+1:]

			// Reset buffer
			mentionBuf = ""
			mentionInd = 0
		}
	}

	return s
}

func stringsCmp(str string, cmp ...string) bool {
	for _, c := range cmp {
		if c == str {
			return true
		}
	}

	return false
}
