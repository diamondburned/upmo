package utils

// Sanitize escapes code blocks and everyone pings.
func Sanitize(s string) string {
	return EscapeCodeBlock(EscapeEveryone(s))
}
