package utils

import "strings"

var markdownEscaper = strings.NewReplacer(
	"*", "\\*",
	"_", "\\_",
	"~", "\\~",
	"`", "\\`",
)

// EscapeMarkdown escapes the markdown characters.
func EscapeMarkdown(s string) string {
	return markdownEscaper.Replace(strings.ReplaceAll(s, "\\", "\\\\"))
}
