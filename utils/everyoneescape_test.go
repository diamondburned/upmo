package utils

import (
	"fmt"
	"strings"
	"testing"
)

func TestEscapeEveryone(t *testing.T) {
	var samples = [...]string{
		"@everyone",
		"@\u202eeveryone",
	}

	for _, sample := range samples {
		esc := EscapeEveryone(sample)
		if !strings.HasPrefix(esc, "@\u200b") {
			t.Fatal("Escape failed:", fmt.Sprintf("%#v", esc))
		}
	}
}
