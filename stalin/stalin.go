package stalin

import (
	"github.com/asdine/storm"
	"github.com/bwmarrin/discordgo"
)

// Settings contains kv store for settings
type Settings struct {
	GuildID string `storm:"unique,id"`

	BlacklistedWords  []string
	ExceptionChannels []string
}

// Setup sets up a guild
func Setup(d *discordgo.Session, guildID string) error {
	g, err := d.State.Guild(guildID)
	if err != nil {
		return err
	}

	node, err := node.Begin(true)
	if err != nil {
		return err
	}

	defer node.Rollback()

	s := Settings{
		GuildID: g.ID,
	}

	if err := node.Save(&s); err != nil {
		if err != storm.ErrAlreadyExists {
			return err
		}
	}

	return node.Commit()
}

// AddBlacklistedWord adds blacklisted words into the guild's settings
func AddBlacklistedWord(d *discordgo.Session, guildID string, word ...string) error {
	return updateNode(func(node storm.Node) error {
		var s Settings
		if err := node.One("GuildID", guildID, &s); err != nil {
			return err
		}

		s.BlacklistedWords = append(s.BlacklistedWords, word...)
		return node.Save(&s)
	})
}

// RemoveBlacklistedWord removes blacklisted words from the guild's settings
func RemoveBlacklistedWord(d *discordgo.Session, guildID string, word ...string) error {
	return updateNode(func(node storm.Node) error {
		var s Settings
		if err := node.One("GuildID", guildID, &s); err != nil {
			return err
		}

	Main:
		for i, b := range s.BlacklistedWords {
			for _, w := range word {
				if w == b {
					s.BlacklistedWords = append(
						s.BlacklistedWords[:i],
						s.BlacklistedWords[i+1:]...,
					)

					break Main
				}
			}
		}

		return node.Save(&s)
	})
}

// GetBlacklistedWords returns all blacklisted words in the guild's settings
func GetBlacklistedWords(d *discordgo.Session, guildID string) ([]string, error) {
	var s Settings
	if err := node.One("GuildID", guildID, &s); err != nil {
		return nil, err
	}

	return s.BlacklistedWords, nil
}

// SetExceptionChannel adds an exception channel into the guild's settings
func SetExceptionChannel(d *discordgo.Session, guildID, channelID string) error {
	return updateNode(func(node storm.Node) error {
		c, err := d.State.Channel(channelID)
		if err != nil {
			return err
		}

		var s Settings
		if err := node.One("GuildID", guildID, &s); err != nil {
			return err
		}

		for i, ch := range s.ExceptionChannels {
			if ch == c.ID {
				s.ExceptionChannels = append(
					s.ExceptionChannels[:i],
					s.ExceptionChannels[i+1:]...,
				)

				return node.Save(&s)
			}
		}

		s.ExceptionChannels = append(s.ExceptionChannels, c.ID)
		return node.Save(&s)
	})
}

// GetExceptionChannels returns all blacklisted words in the guild's settings
func GetExceptionChannels(d *discordgo.Session, guildID string) ([]string, error) {
	var s Settings
	if err := node.One("GuildID", guildID, &s); err != nil {
		return nil, err
	}

	return s.ExceptionChannels, nil
}

func updateNode(f func(node storm.Node) error) error {
	node, err := node.Begin(true)
	if err != nil {
		return err
	}

	defer node.Rollback()

	if err := f(node); err != nil {
		return err
	}

	return node.Commit()
}
