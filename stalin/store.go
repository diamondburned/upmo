package stalin

import (
	"sync"

	"github.com/bwmarrin/discordgo"
)

type message struct {
	ID      string
	Content string
}

type messageStore struct {
	messages []message
	mu       sync.Mutex
}

var store = &struct {
	sync.Mutex
	Map GuildChannelMessagess
}{
	Map: GuildChannelMessagess{},
}

type GuildChannelMessagess = map[string]ChannelMessages
type ChannelMessages = map[string]*messageStore

func checkInStore(m *discordgo.Message, words []string) []string {
	if m.Author == nil {
		return nil
	}

	store.Lock()

	// If guild isn't in the map, add it
	if _, ok := store.Map[m.GuildID]; !ok {
		store.Map[m.GuildID] = make(map[string]*messageStore)
	}

	// Grab the message store
	s := store.Map[m.GuildID][m.Author.ID]

	// If message store is not initalized
	if s == nil {
		s = &messageStore{}
		store.Map[m.GuildID][m.Author.ID] = s
	}

	store.Unlock()

	// Start locking that individual message store mutex
	s.mu.Lock()
	defer s.mu.Unlock()

	s.messages = append(s.messages, message{
		m.ID, m.Content,
	})

	if len(s.messages) > 25 {
		s.messages = s.messages[25:]
	}

	for _, w := range words {
		ids := wordInSlice(s.messages, w)
		if len(ids) > 0 {
			s.messages = s.messages[:len(s.messages)-len(ids)]
			return ids
		}
	}

	return nil
}
