package stalin

import (
	"testing"
)

type sample struct {
	blacklist string
	messages  []string
	expected  []string
}

func TestWordInSlice(t *testing.T) {
	var samples = [...]sample{
		{"slice", []string{"this", "is", "a", "sl", "ice"},
			[]string{"sl", "ice"}},
		{"slice", []string{"this", "is", "random"},
			nil},
		{"cringe", []string{"cring\u0435"}, // Cyrillic e
			[]string{"cringe"}},
		{"cringe", []string{"crin\u200bge"},
			[]string{"cringe"}}, // 200B no-width space
	}

	var w []string

	for i, sample := range samples {
		w = wordInSlice(
			testGenerateMessages(sample.messages), sample.blacklist)

		cmp(t, sample, w, i+1)
	}
}

func testGenerateMessages(msgs []string) []message {
	mg := make([]message, len(msgs))
	for i, m := range msgs {
		// Filter is at delete.go:CheckDelete()
		m = Filter(m)

		mg[i] = message{
			ID: m, Content: m,
		}
	}

	return mg
}

func testEqualArray(a1, a2 []string) bool {
	if len(a1) != len(a2) {
		return false
	}

	for i := 0; i < len(a1); i++ {
		if a1[i] != a2[i] {
			return false
		}
	}

	return true
}

func cmp(t *testing.T, s sample, result []string, i int) {
	if len(s.expected) == 0 && len(result) == 0 {
		return
	}

	if testEqualArray(s.expected, result) {
		return
	}

	var resultRunes = make([][]rune, len(result))
	for i, r := range result {
		resultRunes[i] = []rune(r)
	}

	var expectedRunes = make([][]rune, len(s.expected))
	for i, r := range s.expected {
		expectedRunes[i] = []rune(r)
	}

	t.Fatalf(`Index: %d
		Test failed on word %s
		Expected %+v (%U)
		Received %+v (%U)`,
		i, s.blacklist,
		s.expected, expectedRunes,
		result, resultRunes)
}
