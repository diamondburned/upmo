package main

import (
	"runtime"
	"runtime/debug"
	"time"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
)

func routerGC(ctx *exrouter.Context) {
	e := &discordgo.MessageEmbed{
		Title: "Go debug info",
		Fields: []*discordgo.MessageEmbedField{
			embedMakeField("Number of goroutines", runtime.NumGoroutine(), false),
			embedMakeField("GOMAXPROCS", runtime.GOMAXPROCS(-1), true),
			embedMakeField("GOOS", runtime.GOOS, true),
			embedMakeField("GOARCH", runtime.GOARCH, true),
			embedMakeField("Go version", runtime.Version(), false),
		},
	}

	var gc debug.GCStats
	debug.ReadGCStats(&gc)

	e.Fields = append(e.Fields,
		embedMakeField("Last garbage collection", gc.LastGC.Format(time.Kitchen), false),
		embedMakeField("Total garbage collections", gc.NumGC, false),
		embedMakeField("Total pauses for GC", gc.PauseTotal, false),
		embedMakeField("Average pause for GC", durationAverage(gc.Pause), false),
	)

	// Get the ping
	start := time.Now()

	m, err := sendToDiscord(ctx, e)
	if err != nil {
		errToDiscord(ctx, errors.Wrap(err, "Failed to send the messsage"))
	}

	duration := time.Now().Sub(start)

	e.Fields = append(e.Fields,
		embedMakeField("Message send latency", duration, false))

	_, err = ctx.Ses.ChannelMessageEditEmbed(ctx.Msg.ChannelID, m.ID, e)
	if err != nil {
		errToDiscord(ctx, errors.Wrap(err, "Failed to edit the message"))
	}

	runtime.GC()
}

func durationAverage(durations []time.Duration) time.Duration {
	var duration time.Duration

	for _, d := range durations {
		duration += d
	}

	duration /= time.Duration(len(durations))

	return duration
}
