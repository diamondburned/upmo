package main

import (
	"fmt"
	"regexp"
	"strconv"

	"github.com/Necroforger/dgrouter/exrouter"
	"gitlab.com/diamondburned/upmo/starboard"
)

var patternChannels = regexp.MustCompile("<#[^>]*>")

func routerStarboard(ctx *exrouter.Context) {
	if ctx.Msg.GuildID == "" {
		sendToDiscord(ctx, "You're not in a guild!")
		return
	}

	switch ctx.Args.Get(1) {
	case "setup":
		a := ctx.Args.Get(2)
		if a == "" {
			sendToDiscord(ctx, "Missing target channel")
			return
		}

		if err := starboard.Setup(ctx.Ses,
			ctx.Msg.GuildID, a[2:len(a)-1]); err != nil {

			errToDiscord(ctx, err)
			return
		}

	case "star":
		var rxn = ctx.Args.Get(2)
		if rxn == "" {
			rxn, err := starboard.GetRxn(ctx.Msg.GuildID)
			if err != nil {
				sendToDiscord(ctx, "Failed to get star reaction: %s"+
					err.Error())
				return
			}

			sendToDiscord(ctx, "Reaction: "+rxn)
			return
		}

		if err := starboard.ChangeRxn(ctx.Ses, ctx.Msg.GuildID, ctx.Args.Get(2)); err != nil {
			errToDiscord(ctx, err)
			return
		}

	case "count":
		a := ctx.Args.Get(2)
		i, err := strconv.Atoi(a)
		if err != nil {
			s, err := starboard.GetStarcount(ctx.Msg.GuildID)
			if err != nil {
				sendToDiscord(ctx, "Failed to get starcount: "+err.Error())
				return
			}

			sendToDiscord(ctx, fmt.Sprintf("Star count: %d", s))
			return
		}

		if !(0 < i) {
			sendToDiscord(ctx, "Count doesn't satisfy `0 < i`")
			return
		}

		if err := starboard.ChangeStarcount(ctx.Ses, ctx.Msg.GuildID, uint(i)); err != nil {
			errToDiscord(ctx, err)
			return
		}

	case "blacklist":
		a := ctx.Args.Get(2)
		if len(a) < 3 {
			bl, err := starboard.GetBlacklisted(ctx.Msg.GuildID)
			if err != nil {
				var s = "Blacklisted channels:"
				for _, ch := range bl {
					s += "<#" + ch + "> "
				}

				sendToDiscord(ctx, s)
				return
			}

			sendToDiscord(ctx, "Missing target channel")
			return
		}

		b, err := starboard.ToggleBlacklist(ctx.Ses, ctx.Msg.GuildID, a[2:len(a)-1])
		if err != nil {
			errToDiscord(ctx, err)
			return
		}

		if b {
			sendToDiscord(ctx, "Added channel to blacklist.")
		} else {
			sendToDiscord(ctx, "Removed channel from blacklist.")
		}

		return

	case "reset":
		if err := starboard.ResetMessage(ctx.Ses, ctx.Msg.GuildID, ctx.Args.Get(2)); err != nil {
			errToDiscord(ctx, err)
			return
		}
	default:
		sendToDiscord(ctx, `Missing command:
	- setup
	- star [emoji]
	- count [int; 0 < i]
	- blacklist [channel]
	- reset [messageID]`)
		return
	}

	sendToDiscord(ctx, "Done.")
}
