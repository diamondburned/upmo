package logger

import (
	"fmt"
	"log"
	"runtime/debug"
	"time"

	"github.com/bwmarrin/discordgo"
)

// Error logs the error
func Error(d *discordgo.Session, guildID string, err error) *discordgo.MessageEmbed {
	if Debug {
		log.Printf("Error in guild %s: %v\n", guildID, err)
	}

	/*
		var traces = make([]string, 0, 3)

		for i := 1; i < 4; i++ {
			p, file, ln, ok := runtime.Caller(i)
			if !ok {
				break
			}

			d := runtime.FuncForPC(p)
			fileparts := strings.Split(file, "/")

			traces = append(traces, fmt.Sprintf(
				"%s#L%d: %s()\n",
				fileparts[len(fileparts)-1], ln, d.Name(),
			))
		}
	*/

	e := &discordgo.MessageEmbed{
		Color:       0xFF0000,
		Description: "```\n" + string(debug.Stack()) + "```",
		Timestamp:   time.Now().Format(time.RFC3339),
	}

	switch err := err.(type) {
	case *discordgo.RESTError:
		if err.Message != nil && err.Message.Message != "" {
			e.Title =
				fmt.Sprintf("%d: %s\n", err.Message.Code, err.Message.Message)
			break
		}

		e.Title = err.Error()
	default:
		e.Title = err.Error()
	}

	if db == nil {
		return e
	}

	var s Settings

	if err := node.One("GuildID", guildID, &s); err != nil {
		return e
	}

	if _, err := d.State.Channel(s.LogChannel); err != nil {
		e.Description = "(Failed to send to log channel)\n" + e.Description
		node.DeleteStruct(&s)

		return e
	}

	d.ChannelMessageSendEmbed(s.LogChannel, e)
	return e
}
