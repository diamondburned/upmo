package logger

import "github.com/asdine/storm"

var (
	db   *storm.DB
	node storm.Node

	Debug bool
)

// Initialize initializes the logger
func Initialize(d *storm.DB) {
	db = d
	node = db.From("logger")
}
