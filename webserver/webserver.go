package webserver

import (
	"git.sr.ht/~diamondburned/gocad"
	"gitlab.com/shihoya-inc/errchi"
)

var r = errchi.NewRouter()

func Register(route string, h errchi.Handler) {
	r.Handle(route, h)
}

func Route(route string, fn func(r *errchi.Router)) {
	r.Route(route, fn)
}

func ListenAndServe(addr string) error {
	return gocad.Serve(addr, r)
}
